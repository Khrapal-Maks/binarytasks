﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ProjectStructure.DAL.Migrations
{
    public partial class SetNullProjectUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 70, new DateTime(2020, 3, 17, 15, 40, 37, 606, DateTimeKind.Unspecified).AddTicks(8615), new DateTime(2022, 5, 2, 14, 24, 31, 418, DateTimeKind.Unspecified).AddTicks(1808), "Error quo repellat neque non iusto soluta alias expedita quis.\nEst sint facilis corporis.\nUt minima dolorum.", "Molestiae cumque autem quam harum maxime dolor.", 13 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2021, 2, 27, 7, 49, 13, 612, DateTimeKind.Unspecified).AddTicks(8472), new DateTime(2021, 5, 14, 21, 40, 11, 240, DateTimeKind.Unspecified).AddTicks(3466), "dicta", "Ut minus quod et esse odio ad.", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 18, new DateTime(2020, 6, 7, 15, 37, 35, 752, DateTimeKind.Unspecified).AddTicks(9293), new DateTime(2021, 7, 11, 12, 4, 7, 83, DateTimeKind.Unspecified).AddTicks(8880), "Fugit est deleniti ut.\nSit ab dolorum.\nEos adipisci autem eligendi sit officia eius aspernatur reiciendis.\nEt tempore ratione aut iste debitis iste est.\nUt culpa ex ipsam hic deserunt eum consequatur sapiente.", "Ea et sunt veritatis ea dolores dolor.", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 76, new DateTime(2020, 12, 6, 14, 46, 4, 269, DateTimeKind.Unspecified).AddTicks(8370), new DateTime(2022, 4, 17, 15, 40, 54, 758, DateTimeKind.Unspecified).AddTicks(4209), "qui", "Qui ea excepturi est quis reprehenderit voluptatum.", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 39, new DateTime(2021, 1, 9, 7, 28, 15, 346, DateTimeKind.Unspecified).AddTicks(8089), new DateTime(2021, 4, 14, 16, 31, 37, 643, DateTimeKind.Unspecified).AddTicks(6066), "quas", "Aut et voluptatem ut in eius vitae." });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 65, new DateTime(2020, 6, 2, 3, 50, 29, 227, DateTimeKind.Unspecified).AddTicks(2496), new DateTime(2021, 4, 16, 21, 48, 0, 573, DateTimeKind.Unspecified).AddTicks(8119), "Ut amet eius.", "Aliquam est et repellendus reiciendis quia facere.", 11 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 45, new DateTime(2021, 1, 30, 11, 34, 45, 289, DateTimeKind.Unspecified).AddTicks(2457), new DateTime(2022, 1, 21, 15, 40, 9, 591, DateTimeKind.Unspecified).AddTicks(7596), "Ut numquam at harum assumenda ut officia eligendi.", "Dicta fugit esse consequatur ipsa aliquid occaecati.", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 15, new DateTime(2020, 10, 31, 2, 18, 4, 225, DateTimeKind.Unspecified).AddTicks(2575), new DateTime(2021, 6, 20, 23, 21, 4, 506, DateTimeKind.Unspecified).AddTicks(3018), "Hic provident possimus doloribus iure omnis numquam autem ea blanditiis. Illo ut officia velit et. Et libero nihil excepturi tempore dolorem sed sapiente velit facilis. Deserunt dolorem delectus accusamus. Eius et aut aut minima sed sint.", "Sequi numquam ea officiis possimus aliquid ea." });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 59, new DateTime(2020, 7, 24, 13, 27, 55, 467, DateTimeKind.Unspecified).AddTicks(8682), new DateTime(2021, 5, 22, 12, 0, 14, 290, DateTimeKind.Unspecified).AddTicks(4304), "corporis", "Aut cum assumenda laudantium quaerat et qui.", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 45, new DateTime(2020, 1, 8, 15, 30, 24, 44, DateTimeKind.Unspecified).AddTicks(2266), new DateTime(2021, 11, 30, 13, 54, 7, 1, DateTimeKind.Unspecified).AddTicks(1862), "Enim corrupti rerum nihil placeat impedit numquam adipisci.\nError eos cupiditate officiis nihil suscipit.\nLabore soluta ut id et id blanditiis magnam.", "Amet magni ab dolor ut corrupti vel.", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 42, new DateTime(2020, 4, 25, 13, 27, 55, 654, DateTimeKind.Unspecified).AddTicks(1476), new DateTime(2021, 7, 26, 15, 4, 58, 816, DateTimeKind.Unspecified).AddTicks(3228), "Mollitia enim beatae et doloribus ipsum. Aliquam ut dignissimos quas magnam cumque ad qui. In doloremque reprehenderit. Delectus ipsa cumque tenetur quia enim.", "Ut aliquam officia ex dicta voluptatem assumenda.", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 45, new DateTime(2020, 1, 30, 19, 2, 34, 971, DateTimeKind.Unspecified).AddTicks(5986), new DateTime(2022, 3, 24, 5, 43, 18, 526, DateTimeKind.Unspecified).AddTicks(8251), "Ex non voluptatem.", "Earum non et quidem totam unde vel.", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 45, new DateTime(2020, 8, 17, 1, 51, 59, 854, DateTimeKind.Unspecified).AddTicks(3414), new DateTime(2021, 6, 14, 10, 33, 7, 442, DateTimeKind.Unspecified).AddTicks(2954), "Porro soluta eveniet.\nTotam autem debitis error voluptatem praesentium.\nFugit molestias omnis error enim ab iusto quisquam.\nNumquam modi facere sapiente reprehenderit omnis aut eum.", "Est accusantium in id in eveniet rem." });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 7, new DateTime(2020, 10, 8, 15, 22, 23, 397, DateTimeKind.Unspecified).AddTicks(9499), new DateTime(2021, 6, 17, 6, 0, 37, 621, DateTimeKind.Unspecified).AddTicks(8451), "quia", "Nihil explicabo temporibus sit et veritatis cupiditate.", 13 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 53, new DateTime(2020, 3, 18, 21, 22, 44, 957, DateTimeKind.Unspecified).AddTicks(9316), new DateTime(2021, 6, 5, 16, 32, 26, 666, DateTimeKind.Unspecified).AddTicks(4584), "Itaque aspernatur nihil qui ab accusantium quis est iste eius. Libero omnis consequuntur. Autem adipisci iusto ipsa. Expedita quis minus et nesciunt expedita ducimus.", "Vel ipsum sunt pariatur eaque quo earum.", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 77, new DateTime(2020, 6, 21, 16, 48, 46, 697, DateTimeKind.Unspecified).AddTicks(557), new DateTime(2021, 5, 14, 0, 54, 9, 907, DateTimeKind.Unspecified).AddTicks(9533), "autem", "Animi et dolore voluptas ea nihil qui.", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 35, new DateTime(2020, 1, 6, 18, 50, 43, 47, DateTimeKind.Unspecified).AddTicks(3970), new DateTime(2021, 6, 16, 13, 55, 9, 503, DateTimeKind.Unspecified).AddTicks(8227), "Consequatur praesentium distinctio reprehenderit in odit a enim. Similique quasi repudiandae aut nihil. Corrupti exercitationem saepe. Magnam ipsum nostrum aut.", "Qui sapiente nam nam rerum quas et.", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 42, new DateTime(2020, 10, 26, 5, 8, 4, 41, DateTimeKind.Unspecified).AddTicks(5935), new DateTime(2022, 3, 22, 14, 3, 25, 356, DateTimeKind.Unspecified).AddTicks(4981), "Sunt ut ducimus aut dolor accusamus beatae atque vitae. Est voluptatem non ut. Perferendis voluptatum architecto et facilis magni sunt qui saepe.", "Id qui et distinctio mollitia mollitia in.", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 33, new DateTime(2020, 8, 14, 22, 14, 48, 274, DateTimeKind.Unspecified).AddTicks(2046), new DateTime(2021, 11, 12, 9, 51, 16, 647, DateTimeKind.Unspecified).AddTicks(3382), "Itaque recusandae dolor. Minima quam blanditiis et soluta voluptates. Quia quia et ea fuga deleniti nobis. Aut commodi tenetur tempore dolorem quibusdam numquam.", "Veniam accusantium fugit dignissimos perspiciatis quia sint.", 5 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 52, new DateTime(2020, 10, 24, 10, 33, 28, 259, DateTimeKind.Unspecified).AddTicks(5405), new DateTime(2021, 3, 18, 21, 37, 55, 883, DateTimeKind.Unspecified).AddTicks(1085), "Dolore sunt ut dolores minus neque.", "Non aut a porro impedit ut nobis.", 12 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 74, new DateTime(2020, 10, 9, 5, 10, 3, 784, DateTimeKind.Unspecified).AddTicks(4162), new DateTime(2021, 12, 2, 10, 1, 0, 533, DateTimeKind.Unspecified).AddTicks(8873), "Maxime sunt aperiam architecto quis aut consequatur non eligendi ad.\nVel vitae at autem porro quia et quidem sint.", "Ut magnam voluptatem placeat voluptatibus voluptatem laborum.", 12 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 65, new DateTime(2020, 8, 1, 16, 50, 26, 578, DateTimeKind.Unspecified).AddTicks(7510), new DateTime(2021, 6, 7, 12, 4, 55, 507, DateTimeKind.Unspecified).AddTicks(3447), "officia", "Sapiente nulla quia minus ducimus id beatae.", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 34, new DateTime(2021, 2, 10, 6, 47, 34, 361, DateTimeKind.Unspecified).AddTicks(938), new DateTime(2021, 3, 20, 18, 44, 30, 538, DateTimeKind.Unspecified).AddTicks(5952), "velit", "Voluptatem sit nisi ut facilis quae vero.", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 15, new DateTime(2020, 12, 2, 20, 56, 8, 416, DateTimeKind.Unspecified).AddTicks(6811), new DateTime(2021, 6, 24, 18, 42, 40, 255, DateTimeKind.Unspecified).AddTicks(657), "neque", "Unde modi beatae dolorem tempora voluptatum velit.", 14 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 58, new DateTime(2020, 5, 15, 23, 27, 46, 966, DateTimeKind.Unspecified).AddTicks(871), new DateTime(2021, 7, 2, 21, 54, 9, 85, DateTimeKind.Unspecified).AddTicks(8578), "voluptatem", "Dolores aliquid quibusdam sint facere quasi atque." });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 60, new DateTime(2020, 8, 21, 21, 29, 36, 583, DateTimeKind.Unspecified).AddTicks(8624), new DateTime(2021, 3, 18, 10, 5, 33, 578, DateTimeKind.Unspecified).AddTicks(9049), "Ipsum aut rerum hic doloribus ipsum.", "Iusto magnam totam ea iste aut optio.", 12 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 27, new DateTime(2020, 9, 19, 0, 42, 16, 511, DateTimeKind.Unspecified).AddTicks(4699), new DateTime(2021, 4, 7, 13, 31, 16, 684, DateTimeKind.Unspecified).AddTicks(4901), "Possimus est totam qui dolorum est voluptates quia rerum voluptatibus.", "Tempore inventore nesciunt placeat suscipit aut quos.", 11 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 75, new DateTime(2020, 2, 27, 3, 19, 46, 973, DateTimeKind.Unspecified).AddTicks(5921), new DateTime(2021, 5, 5, 3, 10, 37, 161, DateTimeKind.Unspecified).AddTicks(3939), "Quia autem explicabo. Voluptatem ducimus laborum sit laudantium libero ducimus quas. Sit voluptatem voluptatem tempora. Quia impedit minima ut.", "Perspiciatis consequatur quia dolores perferendis assumenda numquam.", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 43, new DateTime(2020, 2, 16, 0, 54, 20, 810, DateTimeKind.Unspecified).AddTicks(5792), new DateTime(2021, 6, 4, 0, 42, 7, 712, DateTimeKind.Unspecified).AddTicks(2493), "Illum animi et sed alias.", "Asperiores aliquid magni ut est aperiam cumque.", 14 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 15, new DateTime(2020, 5, 21, 0, 44, 37, 844, DateTimeKind.Unspecified).AddTicks(9218), new DateTime(2021, 5, 19, 18, 48, 7, 789, DateTimeKind.Unspecified).AddTicks(2212), "Et natus sint dolores praesentium et voluptatibus.\nConsequuntur quisquam totam sint fugiat.\nDignissimos fuga aliquid officia nihil ipsum officiis debitis aliquid neque.\nAdipisci omnis mollitia qui ut at enim a sint odit.\nDicta consequatur architecto iure consequuntur.", "Officia autem ut facere perspiciatis repellat officiis.", 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 7, 21, 27, 18, 60, DateTimeKind.Unspecified).AddTicks(9364), "Non ex iste ut at.\nPlaceat qui et nesciunt natus repudiandae.\nConsectetur ut voluptatibus excepturi voluptatem quae ut maxime necessitatibus.\nEt incidunt odio nostrum libero saepe similique.\nEum non quod ipsa qui.", new DateTime(2021, 6, 17, 0, 47, 44, 253, DateTimeKind.Unspecified).AddTicks(8887), "Sapiente quia nemo sint est quis sit.", 6, 18, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 19, 17, 6, 3, 899, DateTimeKind.Unspecified).AddTicks(6782), "Est cumque non dolorem dolorum voluptatem voluptas eligendi.", "Unde deserunt esse sint quae sint sed.", 47, 23, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 13, 22, 56, 45, 783, DateTimeKind.Unspecified).AddTicks(9061), "Repellendus corporis a velit ullam quia aliquid amet. Quia quis incidunt quibusdam officia vero ipsa quia quisquam delectus. Est sed est.", "Est ullam et quia nihil veritatis ratione.", 7, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 1, 10, 10, 4, 468, DateTimeKind.Unspecified).AddTicks(8841), "Nihil voluptatem officia nisi. Quos sint autem praesentium quas ad voluptatem distinctio earum. Voluptas illo recusandae nam et praesentium.", "Laboriosam laboriosam blanditiis ut similique quos et.", 44, 9, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 30, 12, 9, 49, 975, DateTimeKind.Unspecified).AddTicks(1553), "Cumque vel est et et qui impedit in.\nEnim voluptatem cum excepturi unde est iusto dolores magnam.\nPlaceat vitae voluptatem a suscipit alias voluptatem.\nQuasi quisquam est id est dolorem et est.\nMinima aut eaque et placeat earum.\nEa doloribus dolorem tenetur.", "Nulla totam sed quisquam quod qui tempore.", 1, 19, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 1, 13, 29, 57, 264, DateTimeKind.Unspecified).AddTicks(1499), "Quidem soluta sit. Sit at ut qui voluptatem sequi quia non. Repellat eos consequatur animi ut fugiat et veritatis. Esse dolor dolor. Id et quae ipsa voluptas sunt iusto voluptates.", "Nihil inventore dicta et soluta nihil odio.", 48, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 27, 6, 20, 14, 729, DateTimeKind.Unspecified).AddTicks(1899), "voluptate", "Expedita ipsam est fugiat minus ratione rem.", 56, 6 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 16, 7, 13, 12, 919, DateTimeKind.Unspecified).AddTicks(3154), "Fugit rerum omnis modi ipsam aperiam omnis quod.", "Explicabo dicta sed hic natus voluptatem quo.", 66, 27, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId" },
                values: new object[] { new DateTime(2021, 2, 22, 8, 7, 0, 919, DateTimeKind.Unspecified).AddTicks(1487), "Voluptatem quia eius maxime consequatur eveniet a omnis. Ex sed tenetur. Quae inventore excepturi eum.", "Eligendi occaecati vel incidunt nisi enim nihil.", 73 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 4, 23, 20, 37, 232, DateTimeKind.Unspecified).AddTicks(1876), "doloribus", "Animi quaerat sint ea error sequi quisquam.", 17, 20, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 23, 4, 25, 59, 424, DateTimeKind.Unspecified).AddTicks(1051), "Aliquam nihil id voluptatem suscipit quia fugiat odio ut non.", "Quos animi consequatur sunt doloremque debitis repellendus.", 38, 15, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 30, 6, 7, 33, 985, DateTimeKind.Unspecified).AddTicks(9009), "cum", "Corporis nisi odio maxime ut laboriosam aut.", 74, 17, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 30, 15, 7, 43, 14, DateTimeKind.Unspecified).AddTicks(4327), "non", new DateTime(2021, 6, 25, 15, 41, 16, 998, DateTimeKind.Unspecified).AddTicks(8828), "Molestias quaerat velit iste aut officiis quisquam.", 37, 17 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 24, 21, 46, 25, 378, DateTimeKind.Unspecified).AddTicks(5521), "Dolores unde asperiores accusamus aperiam doloremque ut.\nOmnis porro voluptas sit at aut tempore.\nEum sint sunt cum consectetur et est.", null, "Omnis et asperiores molestiae voluptatem itaque nobis.", 31, 17 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 6, 1, 0, 10, 382, DateTimeKind.Unspecified).AddTicks(891), "Aut at ut sit in.", "Unde quo et est aut doloremque qui.", 55, 11, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 2, 15, 2, 56, 35, DateTimeKind.Unspecified).AddTicks(6230), "Non vero eaque velit cum ut.\nOfficiis neque rerum cupiditate voluptatem doloremque omnis laboriosam.\nAut hic voluptates minima non aut eos saepe.\nNisi distinctio exercitationem.", "Consequatur perferendis voluptatem magnam blanditiis aut voluptas.", 5, 25, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 28, 6, 53, 58, 346, DateTimeKind.Unspecified).AddTicks(1768), "Debitis aut sed eius voluptatibus rem repudiandae assumenda eos. Cumque enim accusamus sit repellat ratione quia sit est. Et expedita itaque sit libero est. Eos voluptas provident expedita architecto. Id alias reiciendis ex accusamus commodi amet.", "Deleniti nihil quia voluptatum voluptas dignissimos exercitationem.", 16, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 6, 8, 54, 48, 16, DateTimeKind.Unspecified).AddTicks(4130), "Quia modi rerum quia unde. Magni ab non est neque. Tempore magni sit voluptatem temporibus blanditiis ut.", null, "Quos non iste debitis deleniti optio sed.", 16, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 9, 13, 31, 20, 824, DateTimeKind.Unspecified).AddTicks(5094), "Non nisi error dolor aperiam voluptatem voluptatem maiores.", "Placeat quo praesentium ab laudantium eum qui.", 24, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 2, 4, 56, 46, 178, DateTimeKind.Unspecified).AddTicks(2814), "Non quae non adipisci consequatur dicta quia nam.\nRepellendus adipisci totam optio voluptas similique unde doloribus.\nDolores voluptatibus odio porro sunt non quisquam aut.\nFuga sint molestiae ut placeat cumque minima dolores sint pariatur.\nDolor incidunt nam qui distinctio assumenda error iste ipsum.\nEos repellat rem enim.", "Accusantium et sed molestiae unde praesentium voluptates.", 61, 17, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 15, 1, 53, 16, 896, DateTimeKind.Unspecified).AddTicks(9733), "Assumenda esse consectetur ab cumque voluptatem.\nUt voluptatem ipsa illo aut delectus dolorem et quo.\nSint blanditiis delectus eius eligendi quasi rerum quae ducimus.\nIure et sapiente est sint.", "Rem in rem qui dolorem cumque maxime.", 40, 7, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 22, 10, 57, 15, 62, DateTimeKind.Unspecified).AddTicks(9065), "odio", "Sit vel ullam voluptatem esse fuga eligendi.", 61, 24, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 12, 3, 44, 59, 237, DateTimeKind.Unspecified).AddTicks(1263), "Alias aliquam temporibus totam quo ex qui.", "Qui itaque dolorem quis officia incidunt architecto.", 48, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 15, 9, 33, 37, 532, DateTimeKind.Unspecified).AddTicks(6461), "et", "Ab eligendi hic dolorum eos unde deserunt.", 31, 26, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 3, 10, 10, 0, 7, 755, DateTimeKind.Unspecified).AddTicks(3230), "Consequatur ipsa provident rerum temporibus suscipit.\nNemo vitae officia recusandae perspiciatis possimus doloribus sed voluptatem.\nRerum architecto dignissimos.\nFuga et minus cum modi.", "Autem et voluptatem dolorem harum harum rerum.", 5, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 20, 12, 31, 20, 521, DateTimeKind.Unspecified).AddTicks(3645), "Dolores saepe exercitationem quam quia magnam vero aperiam.\nAtque mollitia dolore rerum sapiente fugit sit.\nQuibusdam ut similique consequatur sed aut magni et enim.\nProvident nulla fuga perspiciatis et quo velit nemo praesentium sit.\nEst molestiae commodi exercitationem hic sit ex porro voluptatem.\nAut a dolor consequatur suscipit ut expedita recusandae.", "Sapiente distinctio facilis tempora qui culpa est.", 77, 16, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 13, 4, 29, 10, 984, DateTimeKind.Unspecified).AddTicks(4019), "et", "Aliquid voluptas amet repellat itaque neque voluptate.", 69, 24, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 17, 12, 59, 28, 729, DateTimeKind.Unspecified).AddTicks(1064), "numquam", "Voluptas sed impedit magnam repellendus omnis repudiandae.", 16, 30, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 4, 24, 16, 33, 34, 354, DateTimeKind.Unspecified).AddTicks(5084), "Odit tenetur vitae. Nemo autem accusantium blanditiis. Aut dolorum quos.", "Aspernatur id facere autem autem ipsum doloremque.", 59, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 26, 20, 18, 21, 814, DateTimeKind.Unspecified).AddTicks(6109), "et", "Eum aut id eos et ullam voluptatem.", 24, 21, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 11, 9, 18, 27, 914, DateTimeKind.Unspecified).AddTicks(833), "Blanditiis amet delectus omnis ipsa fugit id.\nAut optio architecto repellendus ratione voluptatem itaque.\nConsequatur et debitis consequuntur.\nVoluptatem placeat ipsum molestiae vero amet quisquam.", "Soluta rerum et assumenda dolorem id ullam.", 23, 9, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 21, 23, 43, 28, 594, DateTimeKind.Unspecified).AddTicks(7358), "praesentium", "Quibusdam ad voluptatem hic itaque occaecati aut.", 48, 12, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 20, 15, 32, 44, 187, DateTimeKind.Unspecified).AddTicks(4215), "Dolores iusto quibusdam. Amet molestiae rerum impedit laudantium. Deleniti maiores accusamus alias maiores. Quis est temporibus libero.", new DateTime(2021, 6, 6, 2, 29, 2, 782, DateTimeKind.Unspecified).AddTicks(8605), "Eveniet et minima fugiat aperiam autem et.", 17, 18, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 20, 10, 39, 34, 703, DateTimeKind.Unspecified).AddTicks(7811), "Sit excepturi qui dolorem.", "Illo et necessitatibus fugiat unde aut assumenda.", 43, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 19, 3, 36, 13, 951, DateTimeKind.Unspecified).AddTicks(3411), "Eaque autem eum voluptatum ut fugiat dolorem magni voluptatem. Debitis repudiandae iusto quo facilis optio voluptatibus voluptatum ab sed. Et sunt facere qui et ut laborum consequuntur et. Asperiores ipsa architecto. Unde nisi ut et enim nisi quibusdam qui.", "Sed ut qui repudiandae non minima pariatur.", 42, 18, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 10, 5, 25, 59, 474, DateTimeKind.Unspecified).AddTicks(9078), "velit", "Perferendis ut ut ullam quis temporibus reprehenderit.", 67, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 9, 18, 27, 21, 480, DateTimeKind.Unspecified).AddTicks(4260), "Aspernatur aliquid dignissimos incidunt quod est delectus corrupti excepturi aut.", "Mollitia non consequatur reprehenderit tenetur rerum officiis.", 30, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 14, 17, 46, 35, 283, DateTimeKind.Unspecified).AddTicks(4739), "Debitis porro magni autem placeat fuga possimus. Rem aperiam ipsum aut at qui. Et beatae molestiae. Delectus et nostrum autem.", "Necessitatibus sit libero sed est accusantium voluptatem.", 28, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 12, 10, 2, 10, 847, DateTimeKind.Unspecified).AddTicks(714), "Facilis quia id.", "Soluta modi sit nam magnam vitae molestiae.", 23, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 13, 11, 11, 14, 140, DateTimeKind.Unspecified).AddTicks(5122), "Corporis est qui nisi voluptatem cum nobis laboriosam. Adipisci eos consequatur. Et molestiae temporibus accusantium aut ut amet corporis laborum quia. Amet eligendi odit officia omnis non consectetur eum.", "Consectetur quia consequatur voluptatum cupiditate sequi aperiam.", 49, 23, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 18, 17, 22, 58, 986, DateTimeKind.Unspecified).AddTicks(7900), "Aliquam quia unde id magni veritatis quae non aliquid.\nNon saepe nesciunt sunt sunt explicabo culpa unde.\nFacere aut quia.\nEst voluptatem nisi.", "Assumenda soluta vel dolores illo perspiciatis minima.", 23, 24, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 10, 15, 13, 38, 557, DateTimeKind.Unspecified).AddTicks(9881), "qui", "Suscipit sunt modi commodi iusto eveniet ipsa.", 61, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 30, 16, 19, 27, 42, DateTimeKind.Unspecified).AddTicks(651), "Quo quisquam recusandae amet eveniet porro repellendus. Rerum beatae deserunt aliquid a tempore. Eos tempore ab iste voluptatem officiis esse ipsa beatae. Eveniet ratione qui repellendus deleniti ipsum autem est accusantium excepturi. Quas impedit ratione nemo laborum nam optio voluptatibus.", "Sunt placeat officiis cupiditate aut molestiae a.", 20, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 31, 14, 37, 40, 738, DateTimeKind.Unspecified).AddTicks(1483), "At quaerat repellat similique sapiente soluta neque et.", "Minus libero aliquam voluptates velit in cum.", 17, 24 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 23, 23, 28, 35, 407, DateTimeKind.Unspecified).AddTicks(1440), "Consectetur aut rerum sunt sed tempora.\nVel doloremque ut numquam ut.", null, "Ratione harum sapiente et dolore ut non.", 13, 28, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 4, 23, 20, 54, 432, DateTimeKind.Unspecified).AddTicks(4967), "qui", "Et voluptatibus sed architecto pariatur asperiores eos.", 60, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 12, 9, 41, 42, 419, DateTimeKind.Unspecified).AddTicks(4953), "Recusandae rerum nihil porro qui rerum quia.\nExcepturi possimus molestias necessitatibus tempora dolor doloribus culpa adipisci et.\nQuas vitae recusandae quaerat non doloribus aut harum ea tempora.\nAliquam sed omnis quam animi et.\nAd minima explicabo voluptate ab omnis.\nDistinctio voluptatem labore ea.", "Eaque totam culpa laborum nihil quasi saepe.", 14, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 8, 2, 35, 17, 636, DateTimeKind.Unspecified).AddTicks(3093), "Facere dolores quas iure aut consequatur in iste voluptatem.\nSapiente consequatur explicabo doloremque sequi perferendis omnis voluptas facilis incidunt.\nEum quas animi ea velit itaque animi ab.", "Porro consequatur deleniti alias vel non est.", 53, 27 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 3, 13, 24, 5, 43, DateTimeKind.Unspecified).AddTicks(4088), "Tenetur eligendi et enim ducimus nulla consequatur distinctio. Inventore vitae vitae quis harum et maiores optio accusamus reiciendis. Recusandae quis eum doloremque quas minus excepturi.", "Deserunt ullam rerum quod enim ipsa et.", 63, 18, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 11, 2, 47, 27, 541, DateTimeKind.Unspecified).AddTicks(6244), "Nobis natus nam occaecati optio excepturi non aut quo.", "Ea libero est unde voluptates dolores eos.", 18, 25, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 19, 13, 40, 41, 146, DateTimeKind.Unspecified).AddTicks(9583), "Vel vitae nulla.", "Natus omnis qui enim debitis assumenda porro.", 68, 17, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 17, 13, 2, 1, 778, DateTimeKind.Unspecified).AddTicks(852), "Asperiores aut quas odio doloribus.\nEt iste odit non delectus animi.\nQuis est nihil mollitia dolor qui cum.\nAspernatur dolore molestiae.\nEt repellendus quia et quod iure debitis ducimus perspiciatis.\nRepudiandae dolorem et iure fuga consectetur quia nemo facere.", "Asperiores corrupti natus eum modi illum autem.", 7, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 4, 24, 1, 7, 40, 657, DateTimeKind.Unspecified).AddTicks(157), "Animi consectetur tempore voluptatum non commodi ut. Voluptatem architecto mollitia. Mollitia rerum temporibus a corrupti eveniet autem dolores consequatur. Itaque mollitia similique eum occaecati consectetur. Ut ut aut ex magnam ab velit hic quo.", "Est quibusdam modi adipisci quia sit enim.", 15, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 13, 9, 13, 30, 36, DateTimeKind.Unspecified).AddTicks(1172), "optio", "Dolore temporibus deserunt maxime velit minima voluptate.", 3, 7, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 4, 7, 38, 42, 329, DateTimeKind.Unspecified).AddTicks(48), "Voluptas porro molestias ut saepe deserunt consequatur omnis cumque pariatur.", "Sequi aliquam beatae voluptates aspernatur nulla aliquid.", 62, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 14, 21, 12, 15, 67, DateTimeKind.Unspecified).AddTicks(1615), "Deleniti eveniet rerum officiis et. Aspernatur vero nihil. Rerum officiis labore pariatur iure. Nesciunt sint quo magnam est doloribus a odit quasi. Totam id similique. Enim accusantium itaque non et asperiores est dolores.", "Consequatur qui harum labore repellendus deserunt sunt.", 12, 6, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 24, 19, 46, 42, 370, DateTimeKind.Unspecified).AddTicks(3965), "Aut cupiditate occaecati corrupti earum quo et quo. Quos nostrum ut ab et recusandae doloremque voluptatibus. Dolorem accusantium rerum odit et ut. Sed voluptates itaque molestias nobis facere mollitia ullam distinctio et.", "Architecto quo velit magni culpa dolor pariatur.", 35, 23 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 10, 5, 4, 27, 152, DateTimeKind.Unspecified).AddTicks(1241), "Omnis quibusdam praesentium et. Nihil repellat accusantium quia sunt qui. Eos quia rerum id optio libero reprehenderit repellat occaecati laboriosam. Laboriosam ea optio est magni ut non.", "Consequatur sit quaerat reiciendis voluptatem dolores odit.", 5, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 22, 10, 54, 12, 797, DateTimeKind.Unspecified).AddTicks(1479), "Aut ut debitis totam illo aperiam non.", "A rerum exercitationem autem necessitatibus tenetur molestiae.", 60, 19, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 20, 21, 51, 50, 391, DateTimeKind.Unspecified).AddTicks(1526), "Omnis atque ullam sed dolore inventore odio ut dolor odio. Dolores dolorem quibusdam. Deleniti tempora harum. Impedit est autem. Ex et placeat eligendi quia aliquid quia recusandae inventore mollitia. Quae placeat culpa aliquid aut aut dicta.", "Cum non adipisci quia dignissimos quo sit.", 23, 12, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 16, 10, 48, 24, 269, DateTimeKind.Unspecified).AddTicks(39), "Iure perspiciatis rerum illo. Consequatur voluptatem unde id quas perferendis sed. Facere ex eveniet debitis dolorem.", "Perspiciatis iure qui sit delectus corrupti iure.", 17, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 17, 18, 35, 43, 703, DateTimeKind.Unspecified).AddTicks(5722), "Sint consequatur distinctio sed quidem.\nNemo est dolorem doloribus illo.\nConsectetur consequatur sed rerum nam et.\nNatus facilis laudantium a libero natus autem.", "Maiores voluptatem cupiditate ea ullam nesciunt iure.", 58, 7 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 4, 10, 26, 44, 832, DateTimeKind.Unspecified).AddTicks(2374), "voluptatem", "Neque sint tenetur est voluptatem ut explicabo.", 9, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 3, 19, 9, 51, 11, 369, DateTimeKind.Unspecified).AddTicks(4572), "Architecto doloribus facilis illo velit.", "Et nam provident sapiente sint et expedita.", 64, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 8, 5, 6, 2, 952, DateTimeKind.Unspecified).AddTicks(873), "Sint aut aperiam eaque magni libero porro. Doloremque fugiat aut. Velit est voluptas nulla sapiente beatae delectus quos aut quas.", "Dolorem ut quisquam eum dolores et totam.", 62, 26, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 5, 18, 17, 5, 716, DateTimeKind.Unspecified).AddTicks(6363), "reiciendis", "Architecto velit provident doloremque quos provident accusantium.", 79, 11, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 2, 4, 17, 36, 32, 364, DateTimeKind.Unspecified).AddTicks(7161), "aut", "Iure optio maiores et aut possimus enim.", 42, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 26, 20, 57, 58, 868, DateTimeKind.Unspecified).AddTicks(7072), "deserunt", null, "Sit est ducimus aut eum modi nobis.", 32, 17 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 21, 16, 59, 44, 897, DateTimeKind.Unspecified).AddTicks(2276), "Fugit necessitatibus velit ab vitae pariatur sapiente molestiae et.\nAut excepturi ullam expedita ut magni.\nVoluptatem commodi autem qui excepturi dolorum libero minima in.\nDeleniti distinctio atque cupiditate dolorem iusto non suscipit quia.", new DateTime(2021, 6, 19, 3, 31, 21, 302, DateTimeKind.Unspecified).AddTicks(734), "Deleniti accusamus nemo et voluptas aut perferendis.", 28, 30, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "Name", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 12, 17, 44, 54, 774, DateTimeKind.Unspecified).AddTicks(210), "Placeat sed temporibus ea animi enim maxime est nobis accusantium.\nAssumenda possimus ut minima architecto quis.", "Officiis incidunt quas ut ut sunt est.", 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 28, 8, 43, 39, 914, DateTimeKind.Unspecified).AddTicks(8666), "Ipsum qui laudantium quos est fuga est non minus.", "Distinctio inventore neque occaecati veniam fugit et.", 52, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 5, 0, 56, 28, 34, DateTimeKind.Unspecified).AddTicks(7534), "voluptas", new DateTime(2021, 6, 11, 11, 30, 15, 170, DateTimeKind.Unspecified).AddTicks(2118), "Minima ratione aut voluptatum provident a saepe.", 74, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 10, 3, 6, 55, 187, DateTimeKind.Unspecified).AddTicks(9570), "Ut velit et molestiae voluptas. Asperiores recusandae dolore quod omnis perferendis esse. Qui rem esse et voluptas consequatur et ratione nobis. Sit est esse cumque repudiandae recusandae. Quam atque et.", "Et aut voluptatem sequi enim laudantium vero.", 12, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 23, 18, 37, 1, 673, DateTimeKind.Unspecified).AddTicks(6985), "Similique voluptas quae tempora perferendis et officiis.\nExcepturi quia velit.\nIpsum exercitationem sit ipsam.\nEa non et animi magnam ipsum tenetur enim.", "Ea quod illum aut aut ut commodi.", 38, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 30, 22, 44, 14, 262, DateTimeKind.Unspecified).AddTicks(3736), "Fugiat non dicta qui magni illo. Sunt occaecati vitae temporibus. Consequatur dicta pariatur repudiandae voluptatem libero explicabo a. Laudantium ut beatae quia inventore alias perferendis ut dolorem earum. Voluptas tenetur optio est excepturi molestiae. Ut odit non.", "Suscipit fugit voluptate sit et magni architecto.", 4, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 26, 22, 13, 4, 488, DateTimeKind.Unspecified).AddTicks(4871), "Iure aut facilis quasi ab. Corporis ex eos ea. Illo iure nesciunt ea necessitatibus et adipisci pariatur iure. Nostrum dolorem molestias hic soluta soluta ad ex ut.", "Odit quia suscipit doloribus consequatur ea temporibus.", 54, 8, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 21, 22, 35, 5, 584, DateTimeKind.Unspecified).AddTicks(7217), "Saepe voluptas illo.", new DateTime(2021, 6, 18, 16, 27, 37, 824, DateTimeKind.Unspecified).AddTicks(6376), "Corporis et dolor ut pariatur a modi.", 27, 14, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 4, 13, 15, 34, 82, DateTimeKind.Unspecified).AddTicks(9936), "consequatur", "Mollitia accusantium aut similique explicabo sit eos.", 62, 28, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "Name", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 7, 23, 14, 59, 517, DateTimeKind.Unspecified).AddTicks(4632), "Neque atque sit recusandae libero eveniet placeat quas est iusto.\nIpsa itaque ea omnis quia ipsam facilis pariatur atque.\nAdipisci enim excepturi quia reiciendis praesentium.", "Quisquam eos deserunt inventore vero ut odit.", 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 23, 7, 57, 49, 536, DateTimeKind.Unspecified).AddTicks(7841), "Dolorum delectus nisi error.", "Fugit aliquam eos itaque vitae repudiandae magnam.", 66, 21, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 17, 13, 39, 42, 4, DateTimeKind.Unspecified).AddTicks(8128), "Possimus eligendi odio officiis amet alias dolorum est. Et quam cum exercitationem et dolor harum eos cupiditate. Placeat cum dolores illo modi culpa nam aut ex.", "Exercitationem et qui praesentium consequatur vero in.", 19, 28, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 15, 3, 44, 54, 540, DateTimeKind.Unspecified).AddTicks(7929), "Amet necessitatibus quia illum et ea occaecati veniam dolor asperiores.", "Voluptates et quos omnis dolor placeat omnis.", 78, 23 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 14, 19, 35, 34, 922, DateTimeKind.Unspecified).AddTicks(1154), "Veritatis ducimus voluptas ab sunt ea.", "Voluptatem quidem eius nostrum cupiditate ut ut.", 80, 13, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 23, 10, 57, 59, 375, DateTimeKind.Unspecified).AddTicks(5609), "In vero at.\nDolore incidunt eos natus blanditiis magnam.", "Et corrupti id non ea sapiente nemo.", 41, 23, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 9, 14, 32, 41, 180, DateTimeKind.Unspecified).AddTicks(2075), "Fuga molestias expedita nihil vel voluptas numquam. Adipisci modi doloribus quia mollitia. Et nisi voluptatem nostrum expedita.", "Inventore quis pariatur nemo rerum debitis voluptas.", 40, 26, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 6, 23, 0, 35, 726, DateTimeKind.Unspecified).AddTicks(9761), "Dolore eos quod sunt iusto placeat magni aperiam. Atque et molestias. Est similique quas id et eaque modi quidem sed. Vero unde fugiat omnis numquam eius. Repellat nihil rem voluptatum aliquid quam veritatis assumenda.", "Odit accusamus aperiam aut repellat harum adipisci.", 52, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 27, 11, 41, 52, 707, DateTimeKind.Unspecified).AddTicks(1650), "Culpa omnis exercitationem.", "Hic rerum aperiam animi sit voluptatem ut.", 37, 16, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 20, 23, 23, 54, 960, DateTimeKind.Unspecified).AddTicks(5057), "aperiam", "Voluptatem animi aut repudiandae illum odio atque.", 71, 11, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 29, 23, 37, 2, 727, DateTimeKind.Unspecified).AddTicks(640), "Sint cum quisquam et quam nam quia iure quidem. Dolor maxime incidunt. Laborum rerum excepturi sequi voluptas similique rerum. Reprehenderit illo adipisci autem voluptatem animi repudiandae consequuntur quas. Beatae dolor blanditiis. Aspernatur repellendus et non.", "Fugiat repellendus dolores optio dolor qui eos.", 36, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 20, 7, 24, 42, 316, DateTimeKind.Unspecified).AddTicks(7030), "Dolor soluta veniam libero praesentium fuga.\nIn voluptatum excepturi omnis sed libero.\nFugit autem perspiciatis est sunt enim.\nEst ab exercitationem temporibus odit labore suscipit sint.\nIllo quibusdam architecto optio id.\nTenetur molestiae voluptas odio consequuntur minima sint ad perspiciatis.", "Voluptatem sunt iste molestiae vitae ut architecto.", 8, 24, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 12, 23, 4, 10, 301, DateTimeKind.Unspecified).AddTicks(9942), "Aliquid iure veritatis veritatis aut praesentium possimus ipsa aut nulla.\nNihil fugit in et amet.\nMagni est facere est est dolores.\nConsequatur autem nihil sit ea voluptatem quasi.\nSapiente unde reiciendis ab.\nEius velit illum.", "Impedit repellat ut et tempore tempore provident.", 66, 4 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 25, 1, 13, 38, 298, DateTimeKind.Unspecified).AddTicks(7292), "Labore incidunt distinctio aliquam ut magnam qui facere sunt.", new DateTime(2021, 6, 24, 6, 48, 3, 345, DateTimeKind.Unspecified).AddTicks(2859), "Deleniti veniam aut hic sapiente corrupti et.", 21, 14, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 20, 7, 36, 50, 307, DateTimeKind.Unspecified).AddTicks(9266), "Officiis ipsa cupiditate quos. Eius sint omnis tempore nostrum est voluptatem ipsam quibusdam. Delectus nobis veritatis numquam praesentium dolores atque. Dolorem ipsam non commodi consequatur. Soluta ut enim.", new DateTime(2021, 6, 12, 11, 6, 38, 318, DateTimeKind.Unspecified).AddTicks(2302), "Error nihil animi quia quia porro tenetur.", 37, 6, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 17, 2, 59, 2, 266, DateTimeKind.Unspecified).AddTicks(4020), "Et animi labore.\nSaepe placeat sunt.\nIure facere optio quia tempora at omnis quod.", "Labore alias ex est ipsum aut saepe.", 59, 15 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 1, 6, 40, 49, 432, DateTimeKind.Unspecified).AddTicks(5718), "Qui explicabo et quam natus voluptas.\nEst aut cupiditate mollitia tempore asperiores maxime doloremque veritatis.\nEt quia et fugit quas minima et numquam iure.", "Saepe esse accusantium labore ducimus veritatis eos.", 51, 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 25, 15, 45, 8, 275, DateTimeKind.Unspecified).AddTicks(8421), "Rerum laboriosam eum non eaque modi nihil minima quia. Nostrum ea natus saepe ut saepe. Qui sed quia consectetur autem veritatis dolor harum labore. Maiores reprehenderit nihil tenetur.", "Necessitatibus ex nihil consequatur ex id odit.", 67, 18, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 16, 2, 52, 51, 558, DateTimeKind.Unspecified).AddTicks(525), "Provident autem provident id praesentium autem expedita. Sed deserunt voluptatem minima sed ullam quibusdam. Iusto necessitatibus dolor et voluptas sequi quia velit dolores omnis.", "Est sed aliquid in doloribus sapiente exercitationem.", 39, 12, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 5, 19, 20, 57, 947, DateTimeKind.Unspecified).AddTicks(8333), "non", "Eveniet alias eos rerum aspernatur ducimus porro.", 18, 7, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 5, 1, 0, 3, 48, 299, DateTimeKind.Unspecified).AddTicks(2452), "Assumenda non autem.\nSed et nobis sit dolor reprehenderit et.\nVoluptatem esse natus velit ut quaerat amet sapiente.\nDolores ut quasi autem.", null, "Quibusdam sit dolores repudiandae illo omnis aut.", 7, 11, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 11, 18, 23, 18, 677, DateTimeKind.Unspecified).AddTicks(355), "Consequatur et ullam sint delectus dolor aut.\nBeatae ex at molestiae.\nLaboriosam dignissimos eos harum labore nemo et ipsa et.\nMolestiae nobis amet eius sed neque quia consequuntur voluptatum.\nNam dolor porro at et alias saepe.\nSaepe et molestias iste libero.", "Aspernatur sed est quos doloribus explicabo et.", 33, 26, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 29, 4, 7, 16, 567, DateTimeKind.Unspecified).AddTicks(2701), "Dolor a eveniet voluptatem possimus tempora vel voluptates dicta.", "Praesentium adipisci esse qui omnis voluptas quam.", 11, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 19, 23, 23, 4, 835, DateTimeKind.Unspecified).AddTicks(8128), "id", "Labore esse ut voluptatem quia voluptatem quis.", 2, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 29, 11, 3, 5, 787, DateTimeKind.Unspecified).AddTicks(1359), "Vel officiis dolorem veniam quia possimus commodi amet voluptatem sed. Tenetur alias et ea odio aspernatur. Illum quae dicta.", "Recusandae inventore itaque aut esse ullam deleniti.", 70, 2, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 3, 3, 15, 10, 603, DateTimeKind.Unspecified).AddTicks(532), "deleniti", "Quia aut aut maxime nihil provident labore.", 24, 27, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 4, 7, 51, 57, 102, DateTimeKind.Unspecified).AddTicks(9281), "Impedit aut velit voluptatum sapiente. Pariatur rerum rerum ad. At repellendus iure dignissimos saepe officia sed earum occaecati accusantium.", "Adipisci quaerat quis nobis quo repudiandae nihil.", 3, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 16, 17, 58, 13, 851, DateTimeKind.Unspecified).AddTicks(2401), "Ut assumenda modi illo ad quo et hic.", "Natus cum in ut architecto totam enim.", 65, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 3, 1, 55, 35, 106, DateTimeKind.Unspecified).AddTicks(4), "Est eos nam ea et qui id.\nConsequatur soluta officia magni quia tempore libero provident non.\nSaepe architecto id.", "Rem voluptatibus labore earum voluptas modi eaque.", 28, 2, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 13, 17, 42, 44, 821, DateTimeKind.Unspecified).AddTicks(2356), "Vitae dicta reprehenderit officiis suscipit dolor non sunt.", "Eligendi nesciunt a maxime dolore enim minus.", 7, 15, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 15, 13, 51, 7, 654, DateTimeKind.Unspecified).AddTicks(3228), "nulla", new DateTime(2021, 6, 6, 23, 56, 57, 221, DateTimeKind.Unspecified).AddTicks(9334), "Vero similique enim autem nostrum itaque rerum.", 28, 23, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 20, 9, 13, 51, 284, DateTimeKind.Unspecified).AddTicks(7648), "Qui qui inventore voluptas quo eos neque.", "Nostrum omnis quasi error velit at assumenda.", 65, 10 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 26, 21, 59, 8, 910, DateTimeKind.Unspecified).AddTicks(6411), "voluptas", "Quo deserunt est tenetur tenetur sit sunt.", 60, 22, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 20, 8, 12, 26, 329, DateTimeKind.Unspecified).AddTicks(2124), "Est et ut ipsum non ex soluta hic totam atque.\nAut cumque sunt fugit est beatae voluptas.\nMollitia autem et hic quas labore qui molestiae.\nQui cumque illum ipsum esse et repellat ullam.", "Non necessitatibus iusto quas excepturi tenetur eveniet.", 45, 6, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 26, 2, 45, 17, 922, DateTimeKind.Unspecified).AddTicks(4923), "Ut officiis dolores rerum qui dolor.", null, "Neque nulla dolores impedit voluptatibus iste consectetur.", 64, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 11, 8, 54, 59, 346, DateTimeKind.Unspecified).AddTicks(2651), "Vel necessitatibus sint.\nQuo iusto rerum sed et occaecati soluta.", "Odit molestiae voluptatem magni assumenda nam ea.", 75, 22, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 11, 16, 12, 16, 69, DateTimeKind.Unspecified).AddTicks(3874), "Et eos porro. Ipsa culpa alias odit magnam in aut a. Aut eum ut hic blanditiis numquam aliquam.", "Dolorem nobis modi mollitia iusto molestiae animi.", 74, 9 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 1, 9, 53, 39, 581, DateTimeKind.Unspecified).AddTicks(453), "Ut exercitationem incidunt reprehenderit sed itaque soluta. Quo eligendi voluptates accusamus voluptatem fugit non. Reiciendis qui est aut. Accusantium iure molestias repellendus inventore ea ipsa. Dicta ea odit voluptatem maiores eos.", "Earum voluptatibus dolor a facilis dolorem numquam.", 17, 23, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 18, 12, 48, 11, 319, DateTimeKind.Unspecified).AddTicks(3826), "Odio amet quasi dolores porro delectus rerum voluptates. Id qui sed rerum et placeat sit et. Dolorem aut consequuntur sed quam rerum et.", new DateTime(2021, 6, 1, 18, 2, 35, 542, DateTimeKind.Unspecified).AddTicks(6392), "Ad sunt rerum alias inventore corporis architecto.", 21, 17, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 24, 17, 42, 14, 592, DateTimeKind.Unspecified).AddTicks(8741), "Adipisci dolor minima. Fugit id non aut aliquam est et totam nam dicta. Labore assumenda amet harum ipsam voluptas et. Sint necessitatibus repudiandae adipisci ut ipsa at. Eum inventore ratione.", "Quo incidunt laboriosam nostrum voluptatem eveniet quis.", 17, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 22, 10, 58, 15, 844, DateTimeKind.Unspecified).AddTicks(695), "error", "Assumenda omnis totam possimus assumenda animi impedit.", 38, 28, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 12, 2, 48, 55, 561, DateTimeKind.Unspecified).AddTicks(9476), "sed", new DateTime(2021, 6, 26, 3, 24, 20, 55, DateTimeKind.Unspecified).AddTicks(5550), "Aut laboriosam incidunt eligendi ad deserunt voluptas.", 11, 24, 2 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 2, 20, 16, 32, 50, 553, DateTimeKind.Local).AddTicks(6670), "Schinner, Effertz and Kuhic" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 11, 30, 9, 5, 19, 725, DateTimeKind.Local).AddTicks(2752), "Emard, Sipes and Swaniawski" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 4, 29, 21, 19, 56, 872, DateTimeKind.Local).AddTicks(632), "Keeling, Parisian and Bahringer" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 11, 3, 11, 6, 4, 406, DateTimeKind.Local).AddTicks(7339), "Price, Bergstrom and Wilderman" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 9, 13, 4, 11, 43, 10, DateTimeKind.Local).AddTicks(3750), "Mann, Wyman and Zulauf" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 3, 15, 3, 25, 41, 273, DateTimeKind.Local).AddTicks(9590), "Fadel, Lind and Fahey" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 2, 6, 15, 40, 5, 913, DateTimeKind.Local).AddTicks(2283), "Bashirian, Harber and Batz" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 8, 2, 0, 41, 8, 547, DateTimeKind.Local).AddTicks(3875), "Farrell, Denesik and Lakin" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 11, 15, 18, 56, 37, 850, DateTimeKind.Local).AddTicks(3270), "Thiel, Wilkinson and Parker" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 8, 16, 0, 34, 7, 881, DateTimeKind.Local).AddTicks(4806), "Stehr, Wunsch and Gibson" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 4, 20, 14, 15, 57, 25, DateTimeKind.Local).AddTicks(758), "Schimmel, Kshlerin and Maggio" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 11, 3, 0, 57, 53, 312, DateTimeKind.Local).AddTicks(9846), "Legros, Braun and Grant" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 5, 11, 9, 39, 3, 768, DateTimeKind.Local).AddTicks(940), "Moore, Stracke and Kerluke" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 4, 1, 12, 5, 14, 905, DateTimeKind.Local).AddTicks(3990), "Ortiz, Herman and Haley" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 8, 28, 8, 23, 9, 477, DateTimeKind.Local).AddTicks(8392), "Deion30@yahoo.com", "Yasmine", "Davis", new DateTime(2020, 11, 6, 0, 47, 35, 724, DateTimeKind.Local).AddTicks(3272), 11 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2000, 3, 31, 7, 24, 26, 122, DateTimeKind.Local).AddTicks(8898), "Naomie77@gmail.com", "Gerhard", "Hammes", new DateTime(2019, 11, 17, 17, 22, 24, 76, DateTimeKind.Local).AddTicks(9604), 14 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 1, 4, 7, 15, 47, 54, DateTimeKind.Local).AddTicks(9660), "Julian_Nikolaus@gmail.com", "Allie", "Hintz", new DateTime(2019, 12, 3, 4, 49, 51, 843, DateTimeKind.Local).AddTicks(4565), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2012, 4, 19, 22, 57, 32, 622, DateTimeKind.Local).AddTicks(4434), "Hester.Schneider@gmail.com", "Maddison", "Hirthe", new DateTime(2021, 4, 28, 19, 52, 53, 71, DateTimeKind.Local).AddTicks(5362), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 6, 28, 12, 32, 1, 280, DateTimeKind.Local).AddTicks(8300), "Waldo_Jacobson@gmail.com", "Amari", "Stark", new DateTime(2020, 8, 27, 18, 51, 15, 623, DateTimeKind.Local).AddTicks(9728), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2008, 11, 25, 20, 46, 28, 877, DateTimeKind.Local).AddTicks(6382), "Vida92@gmail.com", "Margot", "Abbott", new DateTime(2020, 7, 9, 21, 9, 36, 251, DateTimeKind.Local).AddTicks(8686), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2001, 9, 15, 19, 22, 51, 613, DateTimeKind.Local).AddTicks(5714), "Jessy.Borer@gmail.com", "Alberta", "Paucek", new DateTime(2020, 4, 3, 18, 49, 50, 909, DateTimeKind.Local).AddTicks(4674), 13 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2019, 11, 11, 12, 4, 26, 455, DateTimeKind.Local).AddTicks(896), "Destany_Lemke11@gmail.com", "Torrance", "Prohaska", new DateTime(2021, 2, 18, 9, 38, 19, 101, DateTimeKind.Local).AddTicks(6274), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 3, 12, 22, 23, 49, 621, DateTimeKind.Local).AddTicks(1639), "Thomas_Blick@hotmail.com", "Cruz", "McClure", new DateTime(2021, 6, 12, 22, 10, 2, 527, DateTimeKind.Local).AddTicks(4643), 14 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 6, 2, 1, 53, 11, 825, DateTimeKind.Local).AddTicks(5817), "Marquis.McLaughlin58@gmail.com", "Liza", "Wunsch", new DateTime(2021, 5, 27, 3, 39, 29, 835, DateTimeKind.Local).AddTicks(5944), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2001, 11, 5, 16, 22, 0, 481, DateTimeKind.Local).AddTicks(5201), "Johann_Shields9@gmail.com", "Regan", "Abernathy", new DateTime(2019, 10, 6, 4, 32, 58, 871, DateTimeKind.Local).AddTicks(6366), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1987, 12, 17, 12, 14, 39, 366, DateTimeKind.Local).AddTicks(2421), "Blanca.Larkin99@yahoo.com", "Ola", "Tillman", new DateTime(2020, 9, 10, 21, 9, 49, 809, DateTimeKind.Local).AddTicks(6999), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2015, 1, 5, 22, 26, 55, 168, DateTimeKind.Local).AddTicks(9003), "Gretchen.Kutch@yahoo.com", "Evelyn", "Kris", new DateTime(2020, 8, 25, 0, 6, 6, 213, DateTimeKind.Local).AddTicks(9454), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2006, 4, 17, 2, 10, 3, 87, DateTimeKind.Local).AddTicks(2376), "Cathryn.Graham@gmail.com", "Devyn", "Dare", new DateTime(2020, 4, 4, 15, 47, 59, 139, DateTimeKind.Local).AddTicks(3184), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2015, 9, 18, 14, 23, 6, 568, DateTimeKind.Local).AddTicks(6073), "Madelyn.Parisian@gmail.com", "Levi", "Barrows", new DateTime(2019, 7, 22, 12, 21, 20, 255, DateTimeKind.Local).AddTicks(3380), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 6, 25, 12, 43, 48, 110, DateTimeKind.Local).AddTicks(4508), "Donavon_Hills99@gmail.com", "Arlie", "McLaughlin", new DateTime(2020, 8, 16, 23, 19, 26, 136, DateTimeKind.Local).AddTicks(4658), 13 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2011, 12, 14, 3, 45, 16, 133, DateTimeKind.Local).AddTicks(919), "Ciara.Corkery@hotmail.com", "Marina", "Ratke", new DateTime(2020, 7, 18, 7, 30, 41, 432, DateTimeKind.Local).AddTicks(3375), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2014, 8, 30, 7, 56, 35, 788, DateTimeKind.Local).AddTicks(1599), "Catherine.Bernier@yahoo.com", "Gerald", "Kuhic", new DateTime(2021, 2, 12, 18, 1, 8, 187, DateTimeKind.Local).AddTicks(8834), 11 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2020, 5, 5, 15, 25, 45, 471, DateTimeKind.Local).AddTicks(8661), "Horacio23@hotmail.com", "Vada", "Langosh", new DateTime(2020, 4, 3, 13, 54, 40, 218, DateTimeKind.Local).AddTicks(2495) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2002, 6, 20, 8, 47, 57, 59, DateTimeKind.Local).AddTicks(743), "Walker.Collier17@hotmail.com", "Rita", "Reichert", new DateTime(2019, 10, 16, 8, 18, 10, 599, DateTimeKind.Local).AddTicks(7373) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2009, 9, 21, 14, 28, 31, 689, DateTimeKind.Local).AddTicks(9587), "Joanny.Quitzon27@gmail.com", "Wanda", "Yundt", new DateTime(2021, 3, 18, 15, 28, 27, 655, DateTimeKind.Local).AddTicks(4214), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1993, 11, 27, 17, 0, 4, 301, DateTimeKind.Local).AddTicks(3394), "Lea_Konopelski@gmail.com", "Cassie", "Feil", new DateTime(2020, 1, 20, 5, 16, 8, 298, DateTimeKind.Local).AddTicks(634), 13 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2012, 4, 25, 20, 51, 2, 771, DateTimeKind.Local).AddTicks(174), "Ivah92@gmail.com", "Turner", "Prosacco", new DateTime(2020, 8, 26, 16, 16, 22, 287, DateTimeKind.Local).AddTicks(5286), 11 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1989, 5, 4, 4, 29, 36, 564, DateTimeKind.Local).AddTicks(6330), "Karson.King69@hotmail.com", "Jaquelin", "Crona", new DateTime(2020, 11, 17, 14, 10, 55, 408, DateTimeKind.Local).AddTicks(8681), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2001, 4, 24, 6, 23, 57, 629, DateTimeKind.Local).AddTicks(6982), "Hiram.Becker89@gmail.com", "Diego", "Hyatt", new DateTime(2021, 6, 4, 13, 9, 43, 978, DateTimeKind.Local).AddTicks(9409), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 1, 28, 17, 17, 23, 386, DateTimeKind.Local).AddTicks(256), "Will92@gmail.com", "Aracely", "Upton", new DateTime(2021, 5, 10, 9, 58, 19, 920, DateTimeKind.Local).AddTicks(3773), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2015, 5, 17, 23, 29, 56, 16, DateTimeKind.Local).AddTicks(4597), "Monty_Ziemann@yahoo.com", "Jonathon", "Heathcote", new DateTime(2019, 8, 29, 4, 5, 36, 294, DateTimeKind.Local).AddTicks(8758), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2006, 7, 31, 5, 3, 4, 773, DateTimeKind.Local).AddTicks(1503), "Chaz.Auer82@gmail.com", "Julianne", "Rippin", new DateTime(2020, 6, 2, 23, 40, 38, 535, DateTimeKind.Local).AddTicks(6046), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2017, 8, 15, 18, 45, 36, 360, DateTimeKind.Local).AddTicks(9585), "Bo.McKenzie40@gmail.com", "Lillian", "Kilback", new DateTime(2021, 5, 5, 7, 10, 47, 692, DateTimeKind.Local).AddTicks(2052), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2018, 1, 5, 4, 24, 21, 770, DateTimeKind.Local).AddTicks(8906), "Shanelle_Gusikowski@gmail.com", "Laurel", "Ortiz", new DateTime(2020, 3, 29, 21, 19, 48, 135, DateTimeKind.Local).AddTicks(2673), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 9, 1, 19, 25, 4, 286, DateTimeKind.Local).AddTicks(28), "Austin_Price42@gmail.com", "Malvina", "Wolff", new DateTime(2021, 5, 31, 7, 35, 47, 585, DateTimeKind.Local).AddTicks(2794), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2017, 7, 9, 11, 37, 53, 377, DateTimeKind.Local).AddTicks(4769), "Cleo_Becker54@yahoo.com", "Morris", "Labadie", new DateTime(2020, 4, 19, 20, 39, 21, 726, DateTimeKind.Local).AddTicks(3208), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2016, 10, 4, 3, 35, 40, 245, DateTimeKind.Local).AddTicks(2242), "Margaret.Metz37@yahoo.com", "Katarina", "Heathcote", new DateTime(2020, 4, 23, 3, 19, 48, 647, DateTimeKind.Local).AddTicks(4768), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 10, 29, 2, 23, 38, 349, DateTimeKind.Local).AddTicks(9682), "Reginald62@gmail.com", "Dolly", "Streich", new DateTime(2020, 11, 21, 19, 48, 20, 351, DateTimeKind.Local).AddTicks(9964), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2003, 2, 26, 16, 31, 53, 372, DateTimeKind.Local).AddTicks(6820), "Clementina95@yahoo.com", "Francisca", "Rutherford", new DateTime(2021, 2, 11, 10, 34, 8, 857, DateTimeKind.Local).AddTicks(5159), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1989, 2, 11, 15, 39, 33, 517, DateTimeKind.Local).AddTicks(4375), "Kiel_Powlowski@yahoo.com", "Bernadette", "Larson", new DateTime(2019, 10, 5, 17, 59, 48, 930, DateTimeKind.Local).AddTicks(8), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1994, 3, 1, 16, 31, 13, 938, DateTimeKind.Local).AddTicks(4549), "Bulah_Witting12@gmail.com", "Sherwood", "Kulas", new DateTime(2021, 5, 13, 18, 31, 7, 298, DateTimeKind.Local).AddTicks(5800), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2015, 11, 16, 0, 57, 55, 957, DateTimeKind.Local).AddTicks(7258), "Taya.Schroeder@gmail.com", "Candace", "Yost", new DateTime(2020, 1, 13, 3, 21, 32, 256, DateTimeKind.Local).AddTicks(195), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2000, 9, 24, 11, 54, 37, 643, DateTimeKind.Local).AddTicks(1144), "Diana.Parker@hotmail.com", "Eli", "Cormier", new DateTime(2020, 9, 8, 20, 53, 52, 919, DateTimeKind.Local).AddTicks(9778), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2015, 7, 6, 6, 16, 26, 819, DateTimeKind.Local).AddTicks(1086), "Keeley_Rogahn@gmail.com", "Myrl", "Stiedemann", new DateTime(2020, 8, 24, 10, 41, 11, 7, DateTimeKind.Local).AddTicks(5603), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 12, 19, 6, 19, 43, 855, DateTimeKind.Local).AddTicks(7726), "Ali.Skiles88@gmail.com", "Shanel", "Yost", new DateTime(2020, 6, 27, 18, 37, 27, 566, DateTimeKind.Local).AddTicks(8866), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 5, 1, 4, 55, 47, 919, DateTimeKind.Local).AddTicks(6066), "Samara.OKeefe38@yahoo.com", "Kelli", "VonRueden", new DateTime(2020, 2, 28, 2, 26, 45, 616, DateTimeKind.Local).AddTicks(3523), 14 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2011, 9, 5, 4, 49, 17, 167, DateTimeKind.Local).AddTicks(5233), "Devon60@yahoo.com", "Berenice", "Graham", new DateTime(2021, 3, 30, 10, 6, 35, 287, DateTimeKind.Local).AddTicks(3344), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2018, 4, 5, 0, 5, 2, 7, DateTimeKind.Local).AddTicks(3518), "Karli60@hotmail.com", "Selmer", "Boehm", new DateTime(2020, 5, 25, 1, 29, 9, 978, DateTimeKind.Local).AddTicks(7188), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 1, 2, 0, 22, 21, 800, DateTimeKind.Local).AddTicks(6044), "Chelsey.Kovacek@gmail.com", "Elisa", "Torphy", new DateTime(2019, 7, 23, 9, 41, 7, 897, DateTimeKind.Local).AddTicks(3716), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2011, 9, 12, 5, 6, 55, 552, DateTimeKind.Local).AddTicks(1299), "Rowena_Bernier@hotmail.com", "Susanna", "Crist", new DateTime(2020, 9, 17, 14, 37, 51, 387, DateTimeKind.Local).AddTicks(7878), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2012, 12, 9, 4, 12, 46, 134, DateTimeKind.Local).AddTicks(5476), "Sadye.Padberg@yahoo.com", "Virginie", "Sawayn", new DateTime(2020, 9, 9, 20, 10, 24, 212, DateTimeKind.Local).AddTicks(5927), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2007, 4, 23, 4, 57, 22, 361, DateTimeKind.Local).AddTicks(4269), "Jany.Medhurst9@yahoo.com", "Minerva", "Murphy", new DateTime(2019, 7, 19, 12, 33, 9, 470, DateTimeKind.Local).AddTicks(5095), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2011, 6, 19, 3, 11, 3, 760, DateTimeKind.Local).AddTicks(9612), "Stewart49@hotmail.com", "Jarred", "Green", new DateTime(2021, 2, 4, 6, 18, 29, 114, DateTimeKind.Local).AddTicks(3500), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1986, 11, 19, 0, 15, 7, 852, DateTimeKind.Local).AddTicks(6083), "Orie_Abernathy@yahoo.com", "Fay", "Ortiz", new DateTime(2020, 3, 28, 12, 59, 41, 39, DateTimeKind.Local).AddTicks(4060), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2020, 1, 11, 3, 12, 44, 714, DateTimeKind.Local).AddTicks(1217), "Lela3@gmail.com", "General", "Waelchi", new DateTime(2020, 3, 18, 1, 22, 11, 252, DateTimeKind.Local).AddTicks(3495), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2001, 2, 15, 11, 24, 4, 919, DateTimeKind.Local).AddTicks(3539), "Laurine_Kshlerin@gmail.com", "Darrin", "Krajcik", new DateTime(2019, 9, 2, 15, 2, 50, 215, DateTimeKind.Local).AddTicks(6425), 11 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2001, 12, 28, 1, 8, 11, 97, DateTimeKind.Local).AddTicks(254), "Roy.Ondricka83@yahoo.com", "Natasha", "Lang", new DateTime(2019, 7, 30, 0, 0, 53, 313, DateTimeKind.Local).AddTicks(7862), 14 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2001, 6, 11, 1, 17, 47, 556, DateTimeKind.Local).AddTicks(3979), "Lorenz.Bogan19@gmail.com", "Rupert", "Huel", new DateTime(2020, 7, 18, 17, 5, 14, 918, DateTimeKind.Local).AddTicks(2118), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2018, 8, 26, 3, 50, 59, 561, DateTimeKind.Local).AddTicks(8954), "Skylar98@yahoo.com", "Edward", "Larkin", new DateTime(2020, 11, 3, 20, 29, 5, 808, DateTimeKind.Local).AddTicks(5951), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2014, 4, 12, 1, 43, 37, 603, DateTimeKind.Local).AddTicks(4084), "Armani.Witting@yahoo.com", "Rowan", "Zulauf", new DateTime(2020, 7, 31, 9, 46, 40, 776, DateTimeKind.Local).AddTicks(9400), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2016, 1, 13, 7, 40, 30, 165, DateTimeKind.Local).AddTicks(7637), "Ramon2@hotmail.com", "Ocie", "McDermott", new DateTime(2021, 3, 10, 13, 18, 0, 73, DateTimeKind.Local).AddTicks(2204), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 2, 19, 10, 21, 1, 182, DateTimeKind.Local).AddTicks(53), "Gilberto4@yahoo.com", "Josie", "Nader", new DateTime(2019, 7, 25, 4, 57, 38, 519, DateTimeKind.Local).AddTicks(5777), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2008, 9, 29, 18, 32, 36, 157, DateTimeKind.Local).AddTicks(2232), "Madeline.Johnson@yahoo.com", "Norene", "Klein", new DateTime(2021, 2, 6, 10, 56, 5, 905, DateTimeKind.Local).AddTicks(2539), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2000, 5, 10, 6, 50, 39, 827, DateTimeKind.Local).AddTicks(8899), "Lewis66@gmail.com", "Keegan", "Yost", new DateTime(2019, 12, 16, 9, 59, 8, 213, DateTimeKind.Local).AddTicks(6403), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 3, 17, 7, 47, 40, 320, DateTimeKind.Local).AddTicks(3878), "Jettie_Lindgren@gmail.com", "Harry", "Daniel", new DateTime(2020, 5, 2, 13, 6, 1, 185, DateTimeKind.Local).AddTicks(3028), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2011, 4, 25, 23, 30, 5, 272, DateTimeKind.Local).AddTicks(2038), "Zola_Hilpert50@hotmail.com", "Jackie", "Kerluke", new DateTime(2021, 4, 6, 20, 56, 58, 361, DateTimeKind.Local).AddTicks(9304), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2016, 11, 19, 10, 57, 8, 437, DateTimeKind.Local).AddTicks(6061), "Parker_Wiegand53@yahoo.com", "Travon", "Cruickshank", new DateTime(2020, 11, 12, 10, 51, 52, 473, DateTimeKind.Local).AddTicks(9558), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 3, 7, 8, 4, 24, 604, DateTimeKind.Local).AddTicks(3185), "Benedict.Dare79@yahoo.com", "Kyler", "Quitzon", new DateTime(2021, 2, 4, 17, 47, 16, 374, DateTimeKind.Local).AddTicks(5957), 14 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 3, 21, 23, 42, 13, 293, DateTimeKind.Local).AddTicks(2817), "Margarette_Cole@gmail.com", "Laurine", "McKenzie", new DateTime(2020, 3, 6, 6, 5, 22, 374, DateTimeKind.Local).AddTicks(5280), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2002, 1, 22, 19, 24, 3, 174, DateTimeKind.Local).AddTicks(9250), "Pauline.Blanda79@yahoo.com", "Beryl", "Carroll", new DateTime(2021, 3, 21, 9, 37, 51, 782, DateTimeKind.Local).AddTicks(2756), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2015, 4, 22, 18, 41, 28, 553, DateTimeKind.Local).AddTicks(5763), "Florencio_Kuphal12@gmail.com", "Tobin", "Shields", new DateTime(2021, 4, 13, 4, 22, 49, 853, DateTimeKind.Local).AddTicks(3794), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1989, 7, 22, 0, 17, 0, 600, DateTimeKind.Local).AddTicks(363), "Hanna.Nienow22@gmail.com", "Clemmie", "Bins", new DateTime(2020, 8, 4, 13, 16, 19, 864, DateTimeKind.Local).AddTicks(2098), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2003, 3, 1, 0, 38, 19, 34, DateTimeKind.Local).AddTicks(9785), "Dorian_Schneider@hotmail.com", "Ed", "King", new DateTime(2021, 7, 4, 0, 15, 6, 254, DateTimeKind.Local).AddTicks(6316), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1988, 1, 9, 16, 55, 12, 66, DateTimeKind.Local).AddTicks(2196), "Nikita53@gmail.com", "Gail", "Dach", new DateTime(2020, 1, 18, 12, 6, 52, 740, DateTimeKind.Local).AddTicks(2988), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2004, 6, 17, 13, 41, 37, 867, DateTimeKind.Local).AddTicks(4996), "Ryley8@gmail.com", "Brice", "Hartmann", new DateTime(2019, 10, 20, 15, 50, 55, 698, DateTimeKind.Local).AddTicks(5803), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2017, 7, 25, 17, 45, 7, 166, DateTimeKind.Local).AddTicks(1027), "Jaida.Kunde@yahoo.com", "Flo", "Nitzsche", new DateTime(2021, 3, 31, 6, 1, 42, 190, DateTimeKind.Local).AddTicks(297), 14 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2004, 12, 14, 11, 20, 57, 566, DateTimeKind.Local).AddTicks(182), "Warren_Volkman@gmail.com", "Jay", "Johnston", new DateTime(2021, 6, 18, 2, 24, 58, 105, DateTimeKind.Local).AddTicks(8296), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2002, 8, 15, 16, 53, 13, 282, DateTimeKind.Local).AddTicks(8407), "Edd_Bode97@yahoo.com", "Milton", "Graham", new DateTime(2021, 5, 9, 7, 25, 28, 549, DateTimeKind.Local).AddTicks(9116), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2015, 3, 9, 16, 58, 29, 944, DateTimeKind.Local).AddTicks(3904), "Demarco77@hotmail.com", "Abbigail", "Kuvalis", new DateTime(2020, 3, 25, 2, 25, 14, 171, DateTimeKind.Local).AddTicks(3000), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2017, 5, 26, 16, 32, 40, 66, DateTimeKind.Local).AddTicks(9185), "Theodora_OKeefe@hotmail.com", "Hollis", "Trantow", new DateTime(2020, 7, 30, 6, 52, 22, 758, DateTimeKind.Local).AddTicks(3664), 11 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2012, 10, 4, 19, 24, 44, 728, DateTimeKind.Local).AddTicks(7113), "Otilia_Beatty@yahoo.com", "Sedrick", "Torphy", new DateTime(2020, 9, 23, 1, 20, 43, 167, DateTimeKind.Local).AddTicks(518), 14 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 12, 31, 13, 16, 57, 523, DateTimeKind.Local).AddTicks(5016), "Chauncey14@hotmail.com", "Lavinia", "Beatty", new DateTime(2019, 10, 27, 7, 35, 55, 999, DateTimeKind.Local).AddTicks(948), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2014, 4, 29, 13, 10, 7, 133, DateTimeKind.Local).AddTicks(2717), "Hugh.Lemke18@yahoo.com", "Blanche", "Kemmer", new DateTime(2019, 7, 30, 16, 48, 32, 935, DateTimeKind.Local).AddTicks(8722), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2003, 1, 17, 7, 25, 56, 729, DateTimeKind.Local).AddTicks(8481), "Tod6@hotmail.com", "Scot", "Klein", new DateTime(2019, 8, 13, 21, 17, 29, 293, DateTimeKind.Local).AddTicks(7872), 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 6, new DateTime(2020, 10, 18, 8, 55, 19, 150, DateTimeKind.Unspecified).AddTicks(7443), new DateTime(2021, 3, 28, 3, 11, 0, 124, DateTimeKind.Unspecified).AddTicks(2417), "eos", "Consectetur quasi amet et exercitationem et hic.", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 17, new DateTime(2020, 2, 20, 21, 17, 29, 528, DateTimeKind.Unspecified).AddTicks(9040), new DateTime(2021, 4, 2, 14, 6, 7, 644, DateTimeKind.Unspecified).AddTicks(5462), "Ut enim ad aut provident.\nEst sint enim voluptas.\nNon rerum iusto.\nQuaerat enim dolor.", "Quod magni enim modi placeat quam non.", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 24, new DateTime(2020, 8, 10, 20, 50, 14, 203, DateTimeKind.Unspecified).AddTicks(4744), new DateTime(2021, 9, 24, 0, 3, 18, 963, DateTimeKind.Unspecified).AddTicks(7177), "Accusamus ut maiores cupiditate nulla eligendi delectus. Dolorem ut consequatur. Non et fugiat. Corrupti ratione placeat esse. Quia laborum sint.", "Placeat qui asperiores non sit deleniti et.", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 78, new DateTime(2020, 6, 30, 4, 37, 30, 754, DateTimeKind.Unspecified).AddTicks(2522), new DateTime(2021, 3, 25, 11, 23, 39, 484, DateTimeKind.Unspecified).AddTicks(2452), "minus", "Iusto dolorum aut velit libero perferendis sed.", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 32, new DateTime(2021, 2, 9, 17, 38, 36, 286, DateTimeKind.Unspecified).AddTicks(5143), new DateTime(2021, 6, 21, 13, 41, 29, 398, DateTimeKind.Unspecified).AddTicks(7655), "Et corrupti nam. Quasi alias eos voluptatem alias. Qui minima ex nam eos tempora exercitationem consectetur. Quisquam expedita voluptatem expedita ut tempora necessitatibus at quo. Officia qui ducimus sed non ipsum dolor aliquid magnam.", "Iure accusantium tempore accusamus placeat autem ut." });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 76, new DateTime(2020, 4, 9, 18, 53, 56, 939, DateTimeKind.Unspecified).AddTicks(2040), new DateTime(2021, 8, 23, 13, 38, 54, 529, DateTimeKind.Unspecified).AddTicks(4924), "dolor", "Quod non aut tempore consequatur consequatur occaecati.", 3 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 79, new DateTime(2020, 3, 10, 0, 47, 42, 127, DateTimeKind.Unspecified).AddTicks(1250), new DateTime(2021, 7, 18, 15, 19, 1, 793, DateTimeKind.Unspecified).AddTicks(9776), "veniam", "Qui veritatis dolor voluptatem ullam ea velit.", 12 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 55, new DateTime(2020, 4, 1, 8, 37, 30, 436, DateTimeKind.Unspecified).AddTicks(9670), new DateTime(2022, 2, 25, 2, 10, 4, 113, DateTimeKind.Unspecified).AddTicks(3500), "sed", "Quae ut accusantium iusto quisquam odit sint." });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 22, new DateTime(2020, 9, 4, 15, 11, 21, 727, DateTimeKind.Unspecified).AddTicks(7506), new DateTime(2021, 12, 5, 22, 47, 23, 604, DateTimeKind.Unspecified).AddTicks(3354), "est", "Libero non suscipit et consectetur aliquid a.", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 60, new DateTime(2020, 5, 22, 18, 4, 46, 765, DateTimeKind.Unspecified).AddTicks(2628), new DateTime(2021, 9, 10, 22, 53, 46, 180, DateTimeKind.Unspecified).AddTicks(3812), "Impedit officiis repudiandae laboriosam rerum.\nCorporis totam sunt ut sed architecto eum molestiae nemo sit.\nVero praesentium dignissimos ducimus qui consequatur vel magni ipsum delectus.", "Molestiae rerum dolore molestias voluptas facilis aperiam.", 8 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 39, new DateTime(2020, 8, 13, 14, 31, 28, 854, DateTimeKind.Unspecified).AddTicks(3475), new DateTime(2021, 5, 12, 21, 24, 11, 829, DateTimeKind.Unspecified).AddTicks(7393), "Corrupti quo alias at sed quia sed. Est dolor dicta assumenda ut error quas provident non est. Ut sapiente laudantium molestiae velit quasi nulla magni doloribus saepe. Laborum et neque recusandae expedita.", "Alias maiores autem ipsam iusto et et.", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 5, new DateTime(2021, 1, 19, 22, 47, 47, 708, DateTimeKind.Unspecified).AddTicks(2085), new DateTime(2021, 7, 7, 17, 36, 31, 257, DateTimeKind.Unspecified).AddTicks(9514), "vel", "Dolores molestias eveniet ullam eaque incidunt vitae.", 10 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 65, new DateTime(2020, 6, 29, 13, 29, 2, 658, DateTimeKind.Unspecified).AddTicks(2099), new DateTime(2021, 7, 22, 13, 22, 36, 86, DateTimeKind.Unspecified).AddTicks(279), "odit", "Omnis voluptatem velit officia dolor ad consectetur." });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 49, new DateTime(2021, 1, 15, 23, 33, 10, 819, DateTimeKind.Unspecified).AddTicks(882), new DateTime(2021, 7, 27, 20, 52, 45, 334, DateTimeKind.Unspecified).AddTicks(5180), "Consequuntur quis voluptas omnis ut dolor maiores.", "Voluptates aut voluptatem itaque quisquam aliquid fugiat.", 14 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 69, new DateTime(2020, 11, 16, 22, 28, 51, 349, DateTimeKind.Unspecified).AddTicks(7770), new DateTime(2021, 10, 9, 11, 12, 31, 922, DateTimeKind.Unspecified).AddTicks(3042), "Minus est et fugit voluptatem voluptas.", "Harum sed autem quos dolor blanditiis et.", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 37, new DateTime(2020, 8, 12, 14, 56, 52, 839, DateTimeKind.Unspecified).AddTicks(1217), new DateTime(2021, 8, 13, 18, 18, 47, 134, DateTimeKind.Unspecified).AddTicks(75), "sed", "Aspernatur blanditiis quis numquam earum excepturi quis.", 7 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 61, new DateTime(2020, 6, 15, 21, 28, 40, 324, DateTimeKind.Unspecified).AddTicks(8233), new DateTime(2021, 5, 25, 20, 1, 52, 191, DateTimeKind.Unspecified).AddTicks(6556), "sunt", "Ad at ea dignissimos earum eveniet repudiandae.", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 52, new DateTime(2020, 9, 2, 7, 48, 14, 798, DateTimeKind.Unspecified).AddTicks(6821), new DateTime(2021, 5, 27, 7, 14, 43, 918, DateTimeKind.Unspecified).AddTicks(9885), "fugiat", "Suscipit incidunt molestiae reprehenderit eum autem veniam.", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 14, new DateTime(2020, 11, 6, 9, 50, 18, 978, DateTimeKind.Unspecified).AddTicks(2306), new DateTime(2021, 6, 12, 10, 10, 2, 970, DateTimeKind.Unspecified).AddTicks(7774), "Consequatur natus error. Commodi saepe ducimus. Accusamus nihil cumque rerum aut aperiam quia autem. Aspernatur sit dolor necessitatibus cum.", "Quis rerum nemo sit debitis non quae.", 13 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 12, new DateTime(2020, 4, 21, 1, 54, 9, 131, DateTimeKind.Unspecified).AddTicks(2907), new DateTime(2021, 12, 13, 0, 18, 21, 247, DateTimeKind.Unspecified).AddTicks(5696), "Ut vel earum consequuntur qui in praesentium amet in.\nAd consequatur et modi rerum delectus et eos dolores.\nSed veniam eius consequatur cumque qui facere ipsum ut.\nCumque qui iure.\nQuasi quis beatae et et ad enim.\nCommodi corporis consequatur earum quo saepe deserunt eum eius.", "Placeat suscipit fugit sed aliquid fugiat in.", 2 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 23, new DateTime(2020, 10, 8, 2, 22, 55, 372, DateTimeKind.Unspecified).AddTicks(5820), new DateTime(2022, 5, 8, 3, 44, 4, 333, DateTimeKind.Unspecified).AddTicks(8074), "Dolorem nihil itaque voluptatem.", "Deserunt dolores molestiae molestiae facilis nostrum quo.", 14 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 6, new DateTime(2020, 10, 8, 4, 8, 36, 977, DateTimeKind.Unspecified).AddTicks(6970), new DateTime(2021, 6, 18, 10, 21, 31, 127, DateTimeKind.Unspecified).AddTicks(1205), "Fugiat odio repellat qui veniam sed eligendi rerum tempora rerum. Reiciendis alias perferendis nesciunt maiores sint eveniet quod. Dolores aliquam consectetur mollitia molestiae qui doloremque cum dolores libero. Repellendus quo officiis excepturi cumque dolorem accusantium expedita aperiam. Earum id molestiae tempora culpa eligendi et possimus perferendis et.", "Natus optio et sint repellat omnis iste.", 12 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 38, new DateTime(2020, 10, 7, 8, 40, 16, 792, DateTimeKind.Unspecified).AddTicks(6128), new DateTime(2021, 5, 23, 4, 12, 9, 834, DateTimeKind.Unspecified).AddTicks(5075), "Ut magni quas praesentium neque quos labore impedit sapiente.\nSint omnis quis nulla necessitatibus consequuntur sint.\nMolestiae quia libero.\nEa recusandae qui.\nInventore a rerum tenetur magni sit.", "Magnam ea ut cumque dicta sit consequatur.", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 46, new DateTime(2020, 1, 15, 20, 9, 3, 580, DateTimeKind.Unspecified).AddTicks(2215), new DateTime(2021, 7, 17, 2, 1, 50, 767, DateTimeKind.Unspecified).AddTicks(469), "corrupti", "Exercitationem pariatur placeat nihil voluptate est sint.", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name" },
                values: new object[] { 77, new DateTime(2020, 7, 3, 14, 3, 26, 144, DateTimeKind.Unspecified).AddTicks(7854), new DateTime(2021, 8, 22, 18, 2, 0, 286, DateTimeKind.Unspecified).AddTicks(8288), "quod", "Est magni natus dolor sed est similique." });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, new DateTime(2020, 3, 3, 18, 34, 6, 242, DateTimeKind.Unspecified).AddTicks(1414), new DateTime(2022, 4, 19, 12, 46, 15, 795, DateTimeKind.Unspecified).AddTicks(6780), "Repellendus quaerat autem corrupti accusamus sapiente eum recusandae.", "Suscipit aut doloribus provident quia est dolorum.", 6 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 36, new DateTime(2020, 6, 12, 21, 1, 39, 983, DateTimeKind.Unspecified).AddTicks(2191), new DateTime(2021, 5, 23, 23, 11, 37, 843, DateTimeKind.Unspecified).AddTicks(6665), "Facilis soluta dolores qui.", "Aspernatur sed aut quo fugiat quis aut.", 1 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 39, new DateTime(2020, 5, 23, 4, 13, 26, 985, DateTimeKind.Unspecified).AddTicks(6191), new DateTime(2021, 5, 6, 18, 37, 54, 698, DateTimeKind.Unspecified).AddTicks(1429), "Et dolores dolor voluptatem reiciendis.\nConsequatur temporibus quam corrupti saepe.", "Voluptatum esse qui est sint eum labore.", 4 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 52, new DateTime(2020, 7, 9, 7, 18, 13, 657, DateTimeKind.Unspecified).AddTicks(1734), new DateTime(2021, 5, 7, 17, 33, 38, 447, DateTimeKind.Unspecified).AddTicks(5387), "Animi quia veniam.\nMollitia magni ut facere illum.\nEos provident sint.", "Eligendi doloribus eos in laboriosam nisi distinctio.", 9 });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CuratorProjectID", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 14, new DateTime(2020, 7, 19, 19, 27, 54, 558, DateTimeKind.Unspecified).AddTicks(5290), new DateTime(2021, 3, 21, 17, 56, 17, 441, DateTimeKind.Unspecified).AddTicks(1420), "natus", "Laboriosam eos aliquam sunt magni corporis eos.", 14 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 12, 0, 32, 6, 663, DateTimeKind.Unspecified).AddTicks(2386), "Perspiciatis rerum facere neque voluptatum eligendi. Dicta ipsum nam voluptate. Est molestias adipisci voluptatibus.", null, "Assumenda hic soluta soluta quas eos necessitatibus.", 3, 15, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 20, 23, 8, 18, 670, DateTimeKind.Unspecified).AddTicks(1502), "Aut culpa eaque.\nPerspiciatis corrupti unde reiciendis est accusamus ad non doloribus.\nFuga non dolor ipsum corrupti cupiditate repellat ut consectetur.\nLibero necessitatibus odit molestiae ea.\nMolestiae dolores suscipit dolores omnis eum sed molestiae delectus nihil.", "Quod expedita provident eum omnis error voluptas.", 62, 18, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 11, 21, 24, 48, 982, DateTimeKind.Unspecified).AddTicks(7668), "Eaque sunt iure cumque.\nExpedita eum dolorum in.\nRerum dolor mollitia at quaerat quibusdam quia magnam quia.", "Corrupti consequatur libero est sed incidunt iure.", 57, 18, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 13, 12, 57, 56, 680, DateTimeKind.Unspecified).AddTicks(381), "Odit aspernatur reiciendis odio et.\nNemo quia vel deserunt quo possimus nihil numquam.\nTotam voluptate et dolores eum et dolores quia laborum.", "Quos pariatur vero eius vel quisquam ipsum.", 77, 25, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 25, 19, 15, 34, 582, DateTimeKind.Unspecified).AddTicks(7394), "Dolorem et ut blanditiis minima. Dolore minus qui facilis. Repudiandae ipsam ut iure rerum pariatur. Saepe consequatur perspiciatis est et accusamus quia repudiandae amet autem.", "Alias ut ea laboriosam laborum et corrupti.", 35, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 23, 8, 33, 50, 399, DateTimeKind.Unspecified).AddTicks(8583), "Quo tempore aperiam dicta. Eos hic nam est sint eos voluptates. Quia quasi doloremque dolores consequatur ab accusamus officia itaque. Nisi est voluptatem sit facere laudantium officiis fugit molestiae alias.", "Consectetur ab vitae harum atque aut vero.", 5, 22, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 19, 19, 40, 40, 636, DateTimeKind.Unspecified).AddTicks(6110), "minima", "Ut voluptas ab molestiae deleniti iste non.", 67, 23 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 4, 22, 20, 9, 666, DateTimeKind.Unspecified).AddTicks(7886), "Tenetur adipisci sed est in.\nVoluptates iusto labore enim adipisci eveniet ipsa alias dignissimos aut.\nNihil qui ducimus doloremque ad consectetur.", "Voluptas quos sint quos sunt est possimus.", 37, 17, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId" },
                values: new object[] { new DateTime(2021, 4, 4, 22, 25, 40, 575, DateTimeKind.Unspecified).AddTicks(724), "incidunt", "Molestias aut neque alias ratione debitis consequatur.", 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 18, 6, 31, 31, 611, DateTimeKind.Unspecified).AddTicks(8596), "Qui ab unde repudiandae nostrum cum. Consectetur quod omnis. In et beatae enim quae voluptatem aperiam et non eum.", "Consequatur et nam aliquam quaerat rerum qui.", 21, 25, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 28, 8, 47, 47, 165, DateTimeKind.Unspecified).AddTicks(1852), "Voluptas hic mollitia sapiente porro.", "Tempora est itaque placeat dicta quia sunt.", 7, 27, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 8, 20, 25, 24, 781, DateTimeKind.Unspecified).AddTicks(1360), "Quo quidem quibusdam. Libero fugiat et et. Dignissimos expedita id. Earum quis ipsam ea qui inventore voluptatem atque maiores.", "Aperiam sit ut quia officia earum non.", 4, 3, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 27, 1, 13, 21, 173, DateTimeKind.Unspecified).AddTicks(3342), "dolorum", null, "Aut illo voluptatem asperiores sed neque possimus.", 44, 26 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 14, 22, 35, 39, 970, DateTimeKind.Unspecified).AddTicks(3329), "Rerum ad culpa.", new DateTime(2021, 6, 4, 0, 14, 34, 747, DateTimeKind.Unspecified).AddTicks(5194), "Consequuntur est aliquid nesciunt facilis eum et.", 74, 24 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 12, 13, 5, 24, 141, DateTimeKind.Unspecified).AddTicks(9846), "voluptates", "Saepe et ullam quia est perferendis quae.", 69, 16, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 12, 7, 2, 30, 465, DateTimeKind.Unspecified).AddTicks(5478), "Molestiae inventore vitae dolorem. Velit omnis exercitationem expedita omnis voluptas consequatur est. Et ea quidem nemo dicta minus.", "Qui ut sunt non non libero dolores.", 26, 23, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 15, 14, 10, 30, 195, DateTimeKind.Unspecified).AddTicks(9959), "quibusdam", "Corporis molestiae aut incidunt recusandae et reprehenderit.", 57, 22, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 18, 6, 35, 35, 296, DateTimeKind.Unspecified).AddTicks(4361), "Beatae est non doloremque eum id commodi.\nVoluptas suscipit autem eos qui.\nPerspiciatis commodi tempora nesciunt id perferendis.\nDolorum quod omnis autem aut nihil animi eligendi.\nExcepturi sequi provident corporis non molestias.", new DateTime(2021, 6, 17, 22, 48, 20, 421, DateTimeKind.Unspecified).AddTicks(6098), "Nulla exercitationem distinctio dolores velit amet numquam.", 52, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 19, 19, 1, 57, 636, DateTimeKind.Unspecified).AddTicks(5365), "Illo illum possimus delectus praesentium maiores assumenda labore illum ipsam.\nOfficia dignissimos qui modi sit sint rem eaque a.\nMollitia dignissimos qui et deserunt aut dolores consequatur sed.\nPerferendis vero iusto blanditiis.\nEst laboriosam rem occaecati quidem.\nMaxime reiciendis tempore sed.", "Temporibus doloremque qui ut voluptatibus rem quaerat.", 35, 9, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 27, 17, 15, 32, 978, DateTimeKind.Unspecified).AddTicks(5069), "aut", "A temporibus unde debitis dolores praesentium autem.", 53, 2, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 30, 9, 48, 6, 98, DateTimeKind.Unspecified).AddTicks(5248), "Sint voluptatem distinctio pariatur totam voluptates nam consequatur odit ducimus.\nCulpa quis laudantium reiciendis.", "Minima voluptatem placeat qui consequatur consequatur necessitatibus.", 35, 9, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 8, 21, 38, 23, 385, DateTimeKind.Unspecified).AddTicks(9540), "Ut sequi explicabo omnis temporibus aperiam natus. Laborum et voluptatem iusto eos et. Enim cumque omnis veniam placeat impedit quis molestiae a. In delectus a et eum rerum maiores consectetur et error.", "Ut animi unde commodi id laudantium dolorem.", 48, 29, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 9, 14, 33, 55, 808, DateTimeKind.Unspecified).AddTicks(9317), "Totam voluptas quae rerum est accusamus dolorem.\nDolores fugiat reprehenderit et.\nOmnis tempora ut ad et molestiae ullam.\nSed eveniet adipisci at sit aut doloribus.\nDolorum qui expedita veniam esse blanditiis et deserunt ut.", "Et quam dolorem eum illo suscipit et.", 47, 25, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 22, 10, 32, 48, 828, DateTimeKind.Unspecified).AddTicks(5332), "Enim eum consequatur voluptatem accusamus pariatur temporibus. Maiores dolor ex sint eligendi id. Amet explicabo laborum cum non omnis et ut totam.", "Non ut consectetur commodi illum enim sequi.", 6, 12, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 2, 15, 6, 16, 58, 320, DateTimeKind.Unspecified).AddTicks(8405), "Placeat quod iste architecto. Eius impedit esse ad qui in harum. Perspiciatis aut dolores dolor. Deleniti ut doloribus earum nulla ullam.", "Excepturi est quo sunt natus officia ut.", 40, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 22, 7, 46, 0, 995, DateTimeKind.Unspecified).AddTicks(626), "Nostrum soluta possimus vero et omnis repudiandae aut. Exercitationem reiciendis nihil aut ut quam. Sed consequatur eveniet quaerat adipisci velit debitis dignissimos praesentium. Veritatis eligendi at enim.", "A odit reprehenderit rerum omnis et eos.", 22, 11, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 11, 1, 43, 11, 153, DateTimeKind.Unspecified).AddTicks(9299), "Nostrum provident ullam. Laudantium voluptates cumque ab eaque aut. Sunt aut ipsam in atque quos eius. Nesciunt laborum et. Est placeat dolorum vitae quasi occaecati est officiis. Aliquam eum sequi non.", "Sequi quibusdam repellendus magni quis nostrum eum.", 7, 23, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 30, 21, 48, 46, 823, DateTimeKind.Unspecified).AddTicks(8521), "Aut et dolorem ut repellendus iusto corrupti accusamus.", "Sunt et qui et in veritatis asperiores.", 76, 7, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 4, 20, 21, 4, 7, 706, DateTimeKind.Unspecified).AddTicks(3367), "rem", "Et provident et ut nemo fugiat aut.", 66, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 24, 16, 47, 41, 487, DateTimeKind.Unspecified).AddTicks(8338), "Aut impedit tempora rem reprehenderit nam inventore exercitationem.\nEum quia earum aspernatur impedit esse ut incidunt sit.\nQuaerat consectetur rerum ipsum rerum.", "Officia voluptatibus distinctio aut voluptatem nobis fuga.", 7, 30, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 6, 16, 36, 43, 314, DateTimeKind.Unspecified).AddTicks(2158), "Qui dolores velit voluptatem vitae.\nSapiente est aut hic mollitia rerum qui.", "Nihil inventore eius non nostrum vel fuga.", 69, 25, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 29, 10, 31, 20, 232, DateTimeKind.Unspecified).AddTicks(2726), "Repudiandae est quis dicta. Amet accusamus iusto vel pariatur officiis aut error molestiae unde. Aut quaerat sed id laborum et non. Cumque doloribus quod dicta est. Non dignissimos corporis dignissimos ab repellat mollitia eos. Cumque qui quia quos incidunt et sed aut.", "Et sed laboriosam ad recusandae fugiat sed.", 43, 3, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 3, 5, 39, 57, 536, DateTimeKind.Unspecified).AddTicks(3050), "Dignissimos dolor repellendus voluptatem rerum et et sed debitis omnis.", null, "Earum laboriosam quia omnis consequatur iusto repellat.", 77, 11, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 6, 1, 44, 22, 51, DateTimeKind.Unspecified).AddTicks(4733), "Quis impedit ut odit magni.", "Sit odio velit voluptatibus quidem qui perferendis.", 67, 17 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 6, 5, 37, 36, 203, DateTimeKind.Unspecified).AddTicks(9796), "et", "Voluptatum consequatur aliquam aut iure nisi deserunt.", 48, 17, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 29, 19, 2, 14, 724, DateTimeKind.Unspecified).AddTicks(7561), "Rerum fugit nihil et odio quis voluptas quis modi.", "Recusandae iure et sequi fugit mollitia blanditiis.", 31, 6, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 24, 2, 12, 58, 289, DateTimeKind.Unspecified).AddTicks(3589), "Dolor ut est debitis id aut omnis ducimus odit. Cum molestiae qui vel. Ut eius saepe dolore harum cumque amet aut qui facilis.", "Maxime culpa numquam ab et reprehenderit esse.", 44, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 29, 2, 30, 38, 220, DateTimeKind.Unspecified).AddTicks(9267), "In dignissimos reprehenderit aut qui et qui iusto.\nNeque dolorem ipsum occaecati quisquam cupiditate explicabo doloribus necessitatibus.\nUt soluta doloremque porro sint ipsa.\nQuo velit qui dolores quis.", "Veritatis rerum quo est voluptatem dolore animi.", 43, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 18, 4, 52, 28, 368, DateTimeKind.Unspecified).AddTicks(2936), "ut", "Amet enim corporis et non qui atque.", 68, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 23, 12, 22, 8, 307, DateTimeKind.Unspecified).AddTicks(1798), "Rem voluptas pariatur doloremque dolorum et cumque officiis.", "Velit tempora aliquam impedit nostrum quaerat ipsa.", 10, 13, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 12, 4, 16, 49, 313, DateTimeKind.Unspecified).AddTicks(1076), "Voluptatem et quisquam repellat mollitia aut culpa. Totam magni deleniti praesentium quo laudantium sit et sed accusamus. Accusantium molestiae ducimus beatae et eos. Similique nobis odio nulla nulla dignissimos. Repudiandae eligendi maiores nemo dolorum voluptatem qui exercitationem asperiores reiciendis. Qui debitis rerum modi.", "Laboriosam repellendus nobis accusantium pariatur velit adipisci.", 70, 11, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 2, 6, 43, 15, 148, DateTimeKind.Unspecified).AddTicks(1364), "enim", "Sint temporibus magni cumque temporibus deleniti eos.", 27, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 27, 20, 11, 6, 746, DateTimeKind.Unspecified).AddTicks(793), "Et totam vero pariatur voluptatibus quos nisi cum. Quidem sint molestiae. Dolor doloribus adipisci. Atque et molestias in quos ea non ea sit. Qui reiciendis nesciunt.", "Aperiam perspiciatis molestiae est doloremque adipisci culpa.", 23, 19 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 3, 12, 24, 8, 232, DateTimeKind.Unspecified).AddTicks(7817), "velit", "Optio ut non harum sequi distinctio ut.", 5, 29 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 8, 15, 41, 26, 555, DateTimeKind.Unspecified).AddTicks(8215), "Odit non corrupti et rem saepe cumque non consequatur aperiam.", new DateTime(2021, 6, 3, 11, 59, 41, 69, DateTimeKind.Unspecified).AddTicks(9484), "Ratione dolor fugiat consequatur explicabo reiciendis architecto.", 34, 4, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 16, 5, 39, 29, 577, DateTimeKind.Unspecified).AddTicks(4544), "magnam", "Adipisci sit quia ut dolor beatae vel.", 22, 23, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 23, 18, 46, 56, 446, DateTimeKind.Unspecified).AddTicks(5741), "ratione", "Temporibus sed quas vero qui possimus consequuntur.", 12, 21 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 10, 8, 59, 17, 745, DateTimeKind.Unspecified).AddTicks(5878), "Est sed voluptatem et qui assumenda ut quod mollitia.\nIste eos totam provident.", "Atque reiciendis id fugit ea veritatis voluptatem.", 42, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 29, 10, 41, 18, 862, DateTimeKind.Unspecified).AddTicks(7012), "Eos unde vel est in dolorum.", "Ut ab eum sit laudantium distinctio mollitia.", 51, 10, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 10, 4, 55, 48, 143, DateTimeKind.Unspecified).AddTicks(7541), "Sed reiciendis voluptatem id reprehenderit ullam quia. Possimus quasi quos enim sit ut explicabo consectetur dolor in. Occaecati optio aut vel omnis iusto ut aperiam eos et. Laborum eligendi magnam saepe eius nesciunt.", "Necessitatibus qui incidunt voluptates omnis mollitia at.", 46, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 3, 22, 58, 26, 469, DateTimeKind.Unspecified).AddTicks(3387), "Vel minima maiores qui vel in dolorem. Corporis sequi ut maiores et. Praesentium et sit odio consequuntur. Libero nihil voluptatem nihil accusantium eos fugiat error soluta vel. Voluptatem quia et est. Impedit et vitae qui asperiores.", "Dolores quo ut magnam vitae dolorum et.", 22, 8, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 18, 5, 9, 8, 140, DateTimeKind.Unspecified).AddTicks(7832), "Odit a minima quibusdam hic similique animi culpa corrupti maiores. Distinctio consequatur est. Sunt qui ex corporis. Ea non modi optio excepturi necessitatibus velit omnis in non. Reprehenderit dignissimos animi et quis et quidem. Aliquam reprehenderit soluta quis.", "Excepturi ut quidem temporibus minima id et.", 1, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 4, 10, 4, 43, 57, 416, DateTimeKind.Unspecified).AddTicks(1664), "Id culpa veniam veritatis.", "Voluptates distinctio consectetur sit enim blanditiis recusandae.", 49, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 13, 3, 6, 16, 523, DateTimeKind.Unspecified).AddTicks(7531), "Iste quisquam facilis. Aut distinctio quis. Fugit non consectetur ad itaque.", "Odio est numquam assumenda tempora possimus distinctio.", 23, 6, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 5, 17, 29, 7, 638, DateTimeKind.Unspecified).AddTicks(997), "Fugiat nobis et provident quasi ipsa ea autem.\nProvident et omnis fuga est vitae quo aut dicta.", "Pariatur aliquid et rerum error et aut.", 51, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 18, 9, 8, 57, 73, DateTimeKind.Unspecified).AddTicks(2847), "harum", "Ut sed consequatur animi nesciunt dolorem doloribus.", 54, 22, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 22, 23, 43, 21, 898, DateTimeKind.Unspecified).AddTicks(9765), "Consequatur architecto qui exercitationem aut at cupiditate molestiae molestias.", "Id non cum quia incidunt provident dolores.", 37, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 13, 6, 17, 32, 923, DateTimeKind.Unspecified).AddTicks(5094), "Mollitia voluptatem voluptatum et. Sapiente recusandae aliquam error nisi. Fugit id accusamus.", "Repellendus quisquam rem aut labore iure omnis.", 63, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 6, 15, 30, 38, 256, DateTimeKind.Unspecified).AddTicks(6621), "Exercitationem placeat assumenda saepe consequatur. Magni aut voluptas aliquid incidunt delectus sunt quia consequatur qui. Tempore maiores animi sit debitis magnam vero consectetur. Rerum quas aperiam expedita beatae. Nemo officia sapiente illum sunt.", "Impedit molestiae quaerat explicabo quis qui nihil.", 56, 5, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 23, 8, 29, 29, 618, DateTimeKind.Unspecified).AddTicks(8546), "Facere ut illo sit.\nEt repudiandae nihil sed ab fuga.\nLibero ad necessitatibus expedita maiores nesciunt unde magnam praesentium consequatur.\nEos quia nulla totam.", "Saepe aperiam repellendus laudantium maxime aliquid dolores.", 60, 25, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 10, 5, 3, 9, 80, DateTimeKind.Unspecified).AddTicks(6907), "Excepturi assumenda facere fugit tenetur commodi molestias in.\nConsequatur reiciendis quia.\nNobis iure repellendus adipisci.", "Omnis similique excepturi quia quis aliquam aliquam.", 60, 28 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 18, 18, 24, 59, 480, DateTimeKind.Unspecified).AddTicks(1501), "Est incidunt officia hic mollitia molestiae maxime dolorem.\nA illum ex omnis est animi.\nEum itaque qui veritatis ab.\nQuis eos magnam sequi blanditiis ut qui sed natus illo.", "Quaerat et ullam pariatur consequatur nesciunt hic.", 42, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 8, 11, 42, 30, 763, DateTimeKind.Unspecified).AddTicks(9290), "nihil", "Fuga minus temporibus ea ducimus eaque vitae.", 76, 12, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 2, 16, 4, 12, 55, 140, DateTimeKind.Unspecified).AddTicks(6500), "Dignissimos aut deleniti culpa minima qui omnis libero. Ea expedita minus explicabo voluptatem modi et aut eveniet non. Nisi sunt molestiae sit sunt officiis. Sed aliquid sapiente quod repellat ipsum aperiam.", "Perferendis voluptas perferendis itaque laboriosam soluta et.", 20, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 30, 3, 28, 14, 839, DateTimeKind.Unspecified).AddTicks(1941), "Eos quo quisquam libero.", "Et cum earum labore ratione consequuntur officia.", 37, 29, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 13, 20, 10, 37, 636, DateTimeKind.Unspecified).AddTicks(4002), "Nam distinctio natus veritatis aut voluptas et omnis explicabo provident.", "Aliquid ab praesentium aut ea pariatur facilis.", 29, 1, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "State" },
                values: new object[] { new DateTime(2021, 3, 23, 8, 8, 19, 409, DateTimeKind.Unspecified).AddTicks(468), "ut", "Et nostrum facilis nobis ea et nostrum.", 64, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 30, 23, 31, 51, 222, DateTimeKind.Unspecified).AddTicks(1351), "Quisquam ipsum inventore officiis quasi sed quidem sed cupiditate dolore.", new DateTime(2021, 6, 1, 21, 53, 12, 638, DateTimeKind.Unspecified).AddTicks(3831), "Quas ducimus qui eum exercitationem et adipisci.", 27, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 10, 12, 55, 57, 255, DateTimeKind.Unspecified).AddTicks(1376), "Quas ratione aut optio nihil velit sit placeat laboriosam.", null, "Dolorem et est nam et reprehenderit ea.", 62, 22, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CreatedAt", "Description", "Name", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 8, 18, 15, 8, 82, DateTimeKind.Unspecified).AddTicks(1506), "Minus harum distinctio illo ut provident.\nSed ipsam repellendus dolore consequatur molestiae sit itaque dolores.", "Provident iste ipsum autem veritatis et molestiae.", 15 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 8, 18, 14, 50, 382, DateTimeKind.Unspecified).AddTicks(7897), "Est a tempora quam tempore.", "Cupiditate ratione nostrum ut impedit magni quos.", 73, 20, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 22, 18, 21, 57, 47, DateTimeKind.Unspecified).AddTicks(3435), "harum", null, "Hic eius provident delectus aliquam est vel.", 12, 2, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 10, 9, 10, 58, 929, DateTimeKind.Unspecified).AddTicks(7551), "Ipsa non quia.", "Commodi molestiae quia hic qui nisi sed.", 25, 17, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 26, 18, 19, 11, 803, DateTimeKind.Unspecified).AddTicks(9025), "Necessitatibus animi nesciunt unde dolores laboriosam quo.", "Aliquam est officiis qui amet fugit dignissimos.", 76, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 27, 18, 5, 36, 27, DateTimeKind.Unspecified).AddTicks(757), "alias", "Et quisquam dolor quisquam molestiae et nulla.", 25, 18, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 29, 7, 3, 4, 809, DateTimeKind.Unspecified).AddTicks(1153), "Reprehenderit optio quod a quibusdam laborum nihil eligendi odio.", "Numquam vel quas similique fugit sed magnam.", 30, 26, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 9, 2, 3, 15, 749, DateTimeKind.Unspecified).AddTicks(9716), "Dolor quod quibusdam sit fugit fugiat enim est ex.", null, "Provident et voluptatem cupiditate eum enim ut.", 57, 22, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 27, 5, 32, 38, 376, DateTimeKind.Unspecified).AddTicks(8280), "Fuga facilis excepturi pariatur rerum esse officia minus.", "Dicta reiciendis ut alias incidunt rem enim.", 27, 14, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "CreatedAt", "Description", "Name", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 21, 3, 36, 6, 287, DateTimeKind.Unspecified).AddTicks(93), "Ut magnam sed omnis ipsum iste eum eaque autem quasi. Provident sed voluptatem illum voluptatem adipisci. Veritatis dolorum commodi animi eaque minus similique reiciendis aut. Id id necessitatibus non recusandae minus maxime. Est eos et doloremque aliquid.", "Sint repellat neque laudantium dolores accusamus quam.", 13 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 9, 4, 59, 46, 93, DateTimeKind.Unspecified).AddTicks(5398), "Ullam numquam totam quia adipisci placeat nulla.\nConsectetur dolor rerum mollitia eum.", "Maiores aut aut facilis incidunt cum error.", 10, 11, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 81,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 29, 11, 23, 49, 54, DateTimeKind.Unspecified).AddTicks(9855), "Necessitatibus quas molestiae voluptas quibusdam voluptas sed ducimus quae.\nEt ab veniam molestias.", "Laborum doloremque enim nihil quia eligendi laborum.", 27, 19, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 82,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 7, 5, 0, 24, 827, DateTimeKind.Unspecified).AddTicks(1861), "Consequuntur occaecati at.\nAspernatur tenetur tenetur quibusdam voluptatem at ratione.\nDolor a sint iusto cum natus maxime voluptas repellendus.\nProvident iusto adipisci animi amet aut necessitatibus culpa quis explicabo.\nAspernatur voluptatem cupiditate.\nDoloremque tenetur voluptas omnis voluptas.", "Iure dolores est dicta et nisi tenetur.", 15, 5 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 83,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 29, 18, 1, 41, 373, DateTimeKind.Unspecified).AddTicks(3092), "voluptatem", "Ipsam est sunt dolor libero reprehenderit a.", 51, 1, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 84,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 27, 7, 19, 55, 71, DateTimeKind.Unspecified).AddTicks(2281), "Doloribus odit est est quam numquam earum sapiente.\nVoluptate natus veniam sed.", "Et assumenda ratione et soluta deleniti illo.", 35, 13, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 85,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 11, 19, 45, 26, 574, DateTimeKind.Unspecified).AddTicks(8309), "Ea itaque exercitationem eum aut ut ut.", "Eos ut dolor consequuntur sed voluptates animi.", 79, 28, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 86,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 14, 3, 56, 2, 812, DateTimeKind.Unspecified).AddTicks(9278), "Repudiandae adipisci voluptas et. Omnis ullam sint blanditiis adipisci ab sequi eos consequatur consequatur. Reprehenderit iusto expedita quae non et quidem saepe.", "Dolor sed voluptas autem incidunt assumenda consequatur.", 2, 3, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 87,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 3, 11, 19, 50, 765, DateTimeKind.Unspecified).AddTicks(7529), "Inventore qui itaque dolores consequuntur.\nUt et tempore doloremque consequatur error autem sunt numquam quia.\nVoluptatem iste accusamus ullam ipsam et necessitatibus dolorum.\nNostrum voluptatem sint sed voluptatem voluptatem eos veritatis.", "Saepe assumenda et qui labore voluptas corrupti.", 33, 4, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 88,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 4, 13, 13, 1, 977, DateTimeKind.Unspecified).AddTicks(6025), "Sit excepturi et dolorum tempora sit.", "Cupiditate omnis molestiae similique molestiae aspernatur eaque.", 3, 4, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 89,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 4, 20, 15, 32, 56, 552, DateTimeKind.Unspecified).AddTicks(7940), "Ipsam consectetur nihil et cum id.\nRerum minus iure non at nostrum exercitationem a consequatur.", "Ab debitis odio qui est quo eos.", 42, 23 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 90,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 5, 15, 21, 49, 465, DateTimeKind.Unspecified).AddTicks(8869), "ad", "Voluptatum aperiam rerum repellat inventore sequi dicta.", 7, 3, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 91,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 28, 1, 58, 37, 12, DateTimeKind.Unspecified).AddTicks(2759), "Ullam aliquid maiores adipisci officia qui et voluptas.", "Ut beatae qui aut repellat rerum corrupti.", 75, 12 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 92,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 25, 23, 47, 7, 181, DateTimeKind.Unspecified).AddTicks(1584), "Animi eos sint iure eius illum alias at delectus.\nEligendi omnis id quis aut eos error.\nPerspiciatis iusto repellat error assumenda voluptates tenetur quibusdam officia aspernatur.\nNihil fugiat omnis ipsa nihil placeat nulla minima sed molestiae.\nQuo alias quaerat aut sit amet perspiciatis voluptatem optio.", null, "Hic minima omnis impedit aut cupiditate et.", 2, 19, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 93,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 7, 5, 5, 51, 93, DateTimeKind.Unspecified).AddTicks(9339), "ut", null, "Ut debitis eaque quis consequatur iusto quam.", 7, 22, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 94,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 3, 2, 4, 15, 31, 524, DateTimeKind.Unspecified).AddTicks(5132), "Corporis possimus culpa voluptas magnam quia ea quo totam quia.\nAut unde quo voluptatum enim assumenda molestias in.\nDolores porro labore qui totam ut qui provident similique mollitia.\nNisi rerum aut minus laborum molestiae libero dolorem.", "Quisquam enim sed neque culpa commodi perferendis.", 75, 24 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 95,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 26, 16, 33, 39, 773, DateTimeKind.Unspecified).AddTicks(668), "Distinctio vero officia occaecati.\nExercitationem sit mollitia accusamus.\nAccusamus nihil laboriosam dolorem libero.\nEos nulla aspernatur laboriosam tempore et odit quidem qui qui.", "Et aliquam repudiandae omnis dolorem est enim.", 7, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 96,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 19, 0, 33, 34, 910, DateTimeKind.Unspecified).AddTicks(9193), "et", "Quaerat repellat amet sit reiciendis voluptatem qui.", 68, 28, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 97,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 29, 4, 38, 44, 716, DateTimeKind.Unspecified).AddTicks(3536), "Ipsum corrupti incidunt facere hic recusandae. Quod perspiciatis eligendi. Perferendis officiis labore. Consectetur facilis quo est.", "Possimus quia tempora sint maxime explicabo nulla.", 51, 10, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 98,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 2, 4, 10, 2, 271, DateTimeKind.Unspecified).AddTicks(183), "voluptatem", "Et ut et ea tempora vel autem.", 29, 6, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 99,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 29, 15, 14, 39, 382, DateTimeKind.Unspecified).AddTicks(799), "Sit doloribus omnis omnis qui aut illo accusamus.\nVoluptatibus magni molestiae ea occaecati in vel non ipsum.\nRepellat modi quis eius praesentium dignissimos quis facilis.\nQui explicabo qui quod consectetur id aspernatur.\nTotam quaerat molestiae ab ut.", new DateTime(2021, 6, 2, 3, 23, 25, 30, DateTimeKind.Unspecified).AddTicks(6271), "Placeat enim adipisci nesciunt sed harum id.", 56, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 100,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 17, 15, 30, 1, 738, DateTimeKind.Unspecified).AddTicks(6546), "Assumenda ullam aut beatae consequatur odio rem cum qui iure.\nQuibusdam quam quia ut.\nEt qui doloremque corporis rerum sit eveniet quia.\nUt aperiam aut reprehenderit voluptatem possimus in.", "Accusamus tempore culpa iure nihil iusto maiores.", 26, 17, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 101,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 31, 16, 33, 21, 960, DateTimeKind.Unspecified).AddTicks(3290), "beatae", "Ratione nulla nobis provident qui illo rerum.", 14, 18 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 102,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 22, 22, 53, 31, 374, DateTimeKind.Unspecified).AddTicks(4596), "In in voluptate laudantium quis dolore harum distinctio impedit minima.", "Aperiam consectetur ea culpa eius voluptatem officia.", 4, 12, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 103,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 2, 16, 16, 52, 550, DateTimeKind.Unspecified).AddTicks(8088), "Expedita ut ex a aliquid repellat accusamus illum.", "Eveniet et pariatur officia perspiciatis quisquam quae.", 32, 16, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 104,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 18, 16, 16, 59, 239, DateTimeKind.Unspecified).AddTicks(847), "Voluptas vero excepturi quibusdam quas beatae consequatur qui quae nobis.", "Quis quia doloribus voluptas qui perspiciatis esse.", 22, 5, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 105,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 13, 14, 1, 19, 7, DateTimeKind.Unspecified).AddTicks(320), "Consequuntur laborum autem eos dolorem.\nDolor et et id.\nFugit nulla neque pariatur sint et sunt delectus vitae numquam.\nAnimi autem dolores.", "Quos recusandae nostrum minima nam voluptas praesentium.", 60, 21, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 106,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 26, 16, 19, 21, 739, DateTimeKind.Unspecified).AddTicks(3781), "Eveniet quas ipsam vitae rerum vero vel dicta consequuntur molestiae.\nLaboriosam quis et laboriosam voluptatem alias aut repellat.\nDoloribus quae sint debitis perspiciatis.\nQuisquam cum optio quos.", "Cupiditate excepturi ipsum iusto ratione tempore necessitatibus.", 61, 8, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 107,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 29, 10, 51, 14, 804, DateTimeKind.Unspecified).AddTicks(8724), "Alias sit eos ut aspernatur repellat libero debitis nihil.\nSint at officia iusto.\nQuisquam dolores cumque iure quidem totam sed.\nConsectetur natus omnis dolore a.\nDeleniti dolore et et vitae.\nEa dicta sit culpa cumque qui vitae molestias vero voluptatem.", "Quasi in ut ipsa et occaecati sed.", 15, 30, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 108,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 5, 12, 20, 42, 610, DateTimeKind.Unspecified).AddTicks(8432), "Suscipit placeat quo occaecati tempora necessitatibus tempore repellendus quas natus. Quidem hic sed est. Iure accusantium atque molestiae laboriosam omnis dolor blanditiis nihil. Eum rerum qui. Voluptate occaecati dolores sed et tenetur aut natus. Ut officia iusto deleniti et.", "Recusandae iure quam ex laboriosam commodi totam.", 41, 14, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 109,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 29, 23, 31, 31, 169, DateTimeKind.Unspecified).AddTicks(6991), "Aspernatur dolores inventore temporibus illo.\nSequi expedita et maxime debitis id impedit dolorem quod.\nSuscipit et exercitationem debitis consequuntur magni et.\nNumquam quia maxime sapiente dolorem aut error.\nMaiores at laudantium ratione voluptates et vel optio.\nTempore autem eligendi quam recusandae vel voluptatem cumque voluptas.", null, "Libero occaecati reiciendis dignissimos ex voluptatem rerum.", 24, 14, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 110,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 22, 14, 46, 40, 920, DateTimeKind.Unspecified).AddTicks(9358), "corrupti", "Magni ut atque cupiditate sunt nulla ipsa.", 8, 8 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 111,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 25, 21, 25, 56, 308, DateTimeKind.Unspecified).AddTicks(5735), "blanditiis", "At commodi mollitia id sed cupiditate animi.", 61, 6, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 112,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 2, 6, 18, 0, 2, 398, DateTimeKind.Unspecified).AddTicks(7504), "Distinctio iure tempora provident et error. Ea sint tempora sit. Repellendus odit ut sequi quia libero delectus cum harum non. Nostrum qui ducimus molestiae neque delectus. Dolor at dolores accusamus recusandae.", "Qui mollitia voluptatem voluptas nihil fugiat esse.", 23, 4, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 113,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 1, 3, 7, 35, 22, 312, DateTimeKind.Unspecified).AddTicks(2663), "Enim fuga placeat. Quae eos dolores aut itaque vitae quia quidem voluptatem odio. Dignissimos voluptas dolorem repudiandae.", new DateTime(2021, 6, 26, 10, 59, 56, 820, DateTimeKind.Unspecified).AddTicks(9911), "Natus enim non dignissimos rerum aspernatur iusto.", 76, 30 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 114,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 25, 9, 0, 49, 654, DateTimeKind.Unspecified).AddTicks(2424), "Eligendi quisquam doloremque.\nDicta aut et dolore consequatur repudiandae id molestiae dolore quos.\nOdio assumenda laborum eveniet maiores.\nNihil velit accusantium.\nRerum sequi quae ducimus maxime veritatis sunt.", "Ea voluptate nihil dicta non adipisci sit.", 58, 7, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 115,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId" },
                values: new object[] { new DateTime(2021, 2, 7, 4, 24, 16, 959, DateTimeKind.Unspecified).AddTicks(6437), "Deleniti nesciunt non consequatur dicta nihil voluptate id repellendus repellat.", "Esse ea quod ut consequatur est deleniti.", 36, 23 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 116,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 4, 10, 17, 50, 34, 804, DateTimeKind.Unspecified).AddTicks(1221), "exercitationem", "Ducimus at facere quibusdam fuga pariatur neque.", 33, 1, 2 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 117,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 29, 15, 23, 44, 660, DateTimeKind.Unspecified).AddTicks(6755), "deleniti", null, "Officiis asperiores et ullam rerum aut ipsa.", 30, 1, 3 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 118,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 23, 21, 42, 55, 713, DateTimeKind.Unspecified).AddTicks(3846), "Et aut at. Qui quam et laudantium reiciendis inventore. Provident quod ipsum. Quia velit sapiente ea delectus voluptates sint iusto.", "Expedita quo beatae tenetur voluptatum dolores voluptate.", 26, 11, 1 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 119,
                columns: new[] { "CreatedAt", "Description", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 3, 21, 1, 46, 43, 369, DateTimeKind.Unspecified).AddTicks(6367), "quia", "Laboriosam enim optio et consequatur culpa rerum.", 79, 10, 0 });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 120,
                columns: new[] { "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[] { new DateTime(2021, 1, 5, 14, 6, 54, 505, DateTimeKind.Unspecified).AddTicks(3532), "Itaque dignissimos rerum voluptas. Aspernatur sed omnis voluptatem. Iusto cupiditate adipisci veniam odio est. Quibusdam dolorem vel ut ut quod qui ut et aliquam.", null, "Et deleniti quaerat corporis voluptatem quia explicabo.", 51, 7, 0 });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 9, 11, 8, 8, 47, 59, DateTimeKind.Local).AddTicks(6563), "Emard, Conn and Hessel" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 10, 3, 4, 30, 7, 228, DateTimeKind.Local).AddTicks(702), "Walker, Legros and Abshire" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 4, 26, 20, 8, 10, 298, DateTimeKind.Local).AddTicks(4003), "Hartmann, Bartell and Terry" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 1, 12, 17, 28, 0, 232, DateTimeKind.Local).AddTicks(7627), "DuBuque, O'Conner and Haley" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 8, 5, 8, 27, 0, 44, DateTimeKind.Local).AddTicks(3033), "Larkin, Jerde and Grant" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 10, 14, 5, 14, 52, 232, DateTimeKind.Local).AddTicks(3572), "Feest, Emard and Stokes" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 12, 5, 20, 10, 56, 334, DateTimeKind.Local).AddTicks(4904), "Bahringer, Bins and Padberg" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 10, 11, 5, 31, 49, 80, DateTimeKind.Local).AddTicks(385), "Langosh, Ledner and Steuber" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2020, 12, 8, 15, 23, 11, 384, DateTimeKind.Local).AddTicks(4426), "Homenick, Bode and Schulist" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 5, 29, 7, 27, 7, 382, DateTimeKind.Local).AddTicks(8983), "Hamill, Nicolas and Jast" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 1, 15, 7, 32, 30, 245, DateTimeKind.Local).AddTicks(815), "Nader, Bartell and Toy" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 6, 8, 3, 16, 11, 102, DateTimeKind.Local).AddTicks(620), "Kuhlman, Kozey and Hegmann" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 3, 28, 7, 39, 52, 52, DateTimeKind.Local).AddTicks(6305), "Armstrong, Collier and Cruickshank" });

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CreatedAt", "Name" },
                values: new object[] { new DateTime(2021, 7, 4, 9, 52, 27, 901, DateTimeKind.Local).AddTicks(8205), "Hills, Zieme and Parker" });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1999, 2, 21, 15, 3, 47, 155, DateTimeKind.Local).AddTicks(6123), "Raheem80@gmail.com", "Duncan", "Nienow", new DateTime(2019, 12, 6, 3, 28, 23, 759, DateTimeKind.Local).AddTicks(1881), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 12, 8, 2, 21, 43, 22, DateTimeKind.Local).AddTicks(9308), "Concepcion.Hane@gmail.com", "Rod", "Roberts", new DateTime(2021, 3, 12, 22, 57, 24, 725, DateTimeKind.Local).AddTicks(9763), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 8, 4, 1, 15, 49, 374, DateTimeKind.Local).AddTicks(4984), "Gerda.Jacobi67@yahoo.com", "Monroe", "Mitchell", new DateTime(2020, 5, 20, 13, 39, 12, 892, DateTimeKind.Local).AddTicks(3175), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 11, 30, 11, 34, 7, 596, DateTimeKind.Local).AddTicks(1551), "Manley27@yahoo.com", "Keara", "Friesen", new DateTime(2021, 2, 4, 3, 7, 49, 796, DateTimeKind.Local).AddTicks(2144), 14 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2009, 8, 2, 8, 6, 55, 945, DateTimeKind.Local).AddTicks(1349), "Guadalupe24@hotmail.com", "Edd", "Gislason", new DateTime(2021, 2, 23, 3, 47, 16, 293, DateTimeKind.Local).AddTicks(5502), 13 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 6, 14, 22, 17, 40, 98, DateTimeKind.Local).AddTicks(4245), "Jace44@gmail.com", "Samir", "King", new DateTime(2020, 4, 28, 18, 10, 17, 232, DateTimeKind.Local).AddTicks(752), 14 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2017, 1, 5, 20, 34, 15, 35, DateTimeKind.Local).AddTicks(5758), "Freeda.Green@yahoo.com", "Antwan", "Jerde", new DateTime(2021, 1, 7, 20, 39, 20, 873, DateTimeKind.Local).AddTicks(2994), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2016, 8, 13, 13, 23, 48, 451, DateTimeKind.Local).AddTicks(2292), "Hubert_Abbott@yahoo.com", "Chloe", "Orn", new DateTime(2019, 8, 31, 2, 17, 59, 625, DateTimeKind.Local).AddTicks(687), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2016, 7, 16, 6, 17, 53, 356, DateTimeKind.Local).AddTicks(8486), "Demetris64@yahoo.com", "Maximus", "Luettgen", new DateTime(2021, 1, 5, 22, 56, 42, 445, DateTimeKind.Local).AddTicks(6855), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2015, 6, 30, 20, 58, 37, 686, DateTimeKind.Local).AddTicks(4780), "Janessa60@yahoo.com", "Hollie", "Sauer", new DateTime(2020, 6, 28, 5, 2, 34, 720, DateTimeKind.Local).AddTicks(6418), 11 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 8, 1, 4, 4, 13, 904, DateTimeKind.Local).AddTicks(1563), "Murphy.Kulas@hotmail.com", "Quincy", "Torp", new DateTime(2021, 5, 30, 10, 49, 27, 689, DateTimeKind.Local).AddTicks(5660), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2012, 4, 29, 12, 50, 47, 435, DateTimeKind.Local).AddTicks(7848), "Turner_Aufderhar62@yahoo.com", "Lucas", "Ankunding", new DateTime(2021, 3, 12, 16, 36, 46, 533, DateTimeKind.Local).AddTicks(7063), 11 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 2, 22, 9, 42, 8, 912, DateTimeKind.Local).AddTicks(6009), "Valerie.Kulas77@yahoo.com", "Desmond", "Schuster", new DateTime(2021, 4, 7, 15, 49, 49, 535, DateTimeKind.Local).AddTicks(6477), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2011, 4, 15, 12, 22, 3, 881, DateTimeKind.Local).AddTicks(670), "Catalina_Ratke@yahoo.com", "Reynold", "Schoen", new DateTime(2020, 11, 27, 18, 31, 22, 289, DateTimeKind.Local).AddTicks(1637), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2014, 7, 19, 17, 51, 39, 397, DateTimeKind.Local).AddTicks(7139), "Henriette18@gmail.com", "Kirstin", "Lowe", new DateTime(2019, 12, 23, 8, 5, 38, 393, DateTimeKind.Local).AddTicks(2593), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1987, 11, 19, 17, 25, 4, 702, DateTimeKind.Local).AddTicks(702), "Rhoda_Price@gmail.com", "Werner", "Bashirian", new DateTime(2020, 10, 23, 12, 40, 11, 88, DateTimeKind.Local).AddTicks(1175), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2006, 4, 4, 16, 43, 43, 851, DateTimeKind.Local).AddTicks(5687), "Damien_Hyatt@yahoo.com", "Lue", "Brekke", new DateTime(2020, 12, 8, 17, 52, 17, 766, DateTimeKind.Local).AddTicks(8521), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2001, 9, 20, 6, 7, 50, 200, DateTimeKind.Local).AddTicks(3643), "Maggie_Mosciski@hotmail.com", "Malvina", "Greenfelder", new DateTime(2019, 12, 29, 5, 35, 56, 663, DateTimeKind.Local).AddTicks(2914), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2017, 3, 6, 16, 3, 57, 908, DateTimeKind.Local).AddTicks(4200), "Yasmine1@yahoo.com", "Jarret", "Jakubowski", new DateTime(2019, 12, 14, 22, 54, 29, 991, DateTimeKind.Local).AddTicks(9262) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt" },
                values: new object[] { new DateTime(2015, 7, 22, 19, 38, 1, 51, DateTimeKind.Local).AddTicks(7241), "Ericka.Cronin@yahoo.com", "Makenna", "Gulgowski", new DateTime(2021, 2, 24, 23, 59, 21, 822, DateTimeKind.Local).AddTicks(3685) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 4, 8, 20, 15, 24, 221, DateTimeKind.Local).AddTicks(6919), "Casey.Daniel84@gmail.com", "Giovanny", "McCullough", new DateTime(2020, 2, 21, 20, 46, 47, 554, DateTimeKind.Local).AddTicks(2160), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2012, 5, 25, 9, 8, 7, 576, DateTimeKind.Local).AddTicks(7004), "Dora84@yahoo.com", "Wilhelmine", "White", new DateTime(2020, 7, 3, 11, 45, 10, 128, DateTimeKind.Local).AddTicks(5879), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2017, 8, 22, 3, 39, 59, 711, DateTimeKind.Local).AddTicks(8088), "Autumn69@hotmail.com", "Alva", "Bashirian", new DateTime(2020, 3, 14, 17, 38, 55, 898, DateTimeKind.Local).AddTicks(3296), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 10, 12, 13, 54, 17, 791, DateTimeKind.Local).AddTicks(1295), "Sigmund64@hotmail.com", "Angel", "Goodwin", new DateTime(2021, 1, 29, 20, 17, 39, 938, DateTimeKind.Local).AddTicks(8194), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 7, 27, 6, 32, 45, 706, DateTimeKind.Local).AddTicks(6451), "Joana67@gmail.com", "Theresia", "Borer", new DateTime(2020, 12, 16, 19, 33, 57, 958, DateTimeKind.Local).AddTicks(4867), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2014, 5, 10, 15, 53, 49, 445, DateTimeKind.Local).AddTicks(9180), "Rafael17@gmail.com", "Hilario", "Brekke", new DateTime(2020, 5, 17, 3, 13, 41, 617, DateTimeKind.Local).AddTicks(4636), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2019, 2, 22, 4, 3, 41, 169, DateTimeKind.Local).AddTicks(792), "Aliyah.Hyatt@hotmail.com", "Valerie", "Moore", new DateTime(2020, 8, 31, 17, 42, 3, 665, DateTimeKind.Local).AddTicks(4506), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1992, 7, 14, 14, 54, 57, 258, DateTimeKind.Local).AddTicks(9984), "Shana_Mitchell74@yahoo.com", "Barton", "Hintz", new DateTime(2020, 9, 24, 10, 9, 39, 167, DateTimeKind.Local).AddTicks(9815), 13 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2008, 3, 11, 15, 4, 13, 673, DateTimeKind.Local).AddTicks(5109), "Tanner.Olson@yahoo.com", "Karen", "Hansen", new DateTime(2020, 4, 30, 8, 8, 52, 872, DateTimeKind.Local).AddTicks(4347), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2015, 8, 13, 8, 58, 15, 626, DateTimeKind.Local).AddTicks(5596), "Wilber19@hotmail.com", "Korey", "Hayes", new DateTime(2019, 10, 5, 8, 11, 40, 113, DateTimeKind.Local).AddTicks(7021), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1985, 7, 10, 19, 39, 8, 866, DateTimeKind.Local).AddTicks(464), "Amira_Padberg63@gmail.com", "Elinore", "Tremblay", new DateTime(2021, 5, 14, 1, 29, 40, 930, DateTimeKind.Local).AddTicks(2619), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2008, 4, 16, 10, 52, 59, 548, DateTimeKind.Local).AddTicks(5666), "Efrain84@hotmail.com", "Christop", "MacGyver", new DateTime(2019, 10, 2, 14, 30, 4, 147, DateTimeKind.Local).AddTicks(1325), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2011, 6, 10, 15, 33, 37, 434, DateTimeKind.Local).AddTicks(2158), "Brett_Fay@gmail.com", "Kacie", "Wiza", new DateTime(2021, 6, 26, 23, 15, 28, 839, DateTimeKind.Local).AddTicks(1708), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2016, 2, 5, 23, 21, 33, 260, DateTimeKind.Local).AddTicks(2397), "Milan44@hotmail.com", "Monserrate", "Morar", new DateTime(2020, 3, 15, 2, 23, 31, 696, DateTimeKind.Local).AddTicks(5995), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1990, 6, 25, 20, 27, 17, 632, DateTimeKind.Local).AddTicks(7509), "Kelli_Fadel@hotmail.com", "Teagan", "Kshlerin", new DateTime(2020, 8, 17, 5, 28, 21, 580, DateTimeKind.Local).AddTicks(8599), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2001, 1, 5, 12, 3, 30, 139, DateTimeKind.Local).AddTicks(8774), "Lola76@gmail.com", "Alejandrin", "Green", new DateTime(2019, 9, 9, 11, 0, 17, 425, DateTimeKind.Local).AddTicks(5799), 14 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 3, 27, 3, 18, 50, 233, DateTimeKind.Local).AddTicks(5314), "Houston_Legros28@gmail.com", "Patsy", "Hintz", new DateTime(2021, 1, 4, 20, 14, 10, 424, DateTimeKind.Local).AddTicks(4257), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 3, 13, 23, 46, 38, 977, DateTimeKind.Local).AddTicks(2697), "Rosina_Morissette@gmail.com", "Irwin", "Schinner", new DateTime(2021, 3, 8, 20, 49, 10, 269, DateTimeKind.Local).AddTicks(2989), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2017, 5, 4, 2, 20, 52, 560, DateTimeKind.Local).AddTicks(8003), "Micah96@hotmail.com", "Ricky", "Hills", new DateTime(2020, 1, 10, 21, 21, 35, 910, DateTimeKind.Local).AddTicks(3370), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2017, 8, 2, 19, 28, 40, 992, DateTimeKind.Local).AddTicks(501), "Hunter_Mueller@yahoo.com", "Cali", "Ankunding", new DateTime(2021, 4, 27, 11, 33, 37, 74, DateTimeKind.Local).AddTicks(7678), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 4, 5, 17, 24, 37, 98, DateTimeKind.Local).AddTicks(8193), "Tess_Kulas76@gmail.com", "Russell", "Watsica", new DateTime(2021, 1, 7, 18, 38, 56, 497, DateTimeKind.Local).AddTicks(6666), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2016, 12, 1, 6, 29, 52, 883, DateTimeKind.Local).AddTicks(3154), "Rita.Hayes@yahoo.com", "Fernando", "Ratke", new DateTime(2019, 8, 11, 16, 56, 9, 912, DateTimeKind.Local).AddTicks(1755), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2017, 12, 12, 4, 38, 56, 136, DateTimeKind.Local).AddTicks(7832), "Jamel88@gmail.com", "Cloyd", "Leannon", new DateTime(2021, 4, 15, 5, 45, 52, 72, DateTimeKind.Local).AddTicks(3737), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 3, 25, 21, 12, 24, 890, DateTimeKind.Local).AddTicks(8661), "Elijah_Hartmann@gmail.com", "Abigayle", "Gorczany", new DateTime(2020, 11, 25, 6, 19, 44, 977, DateTimeKind.Local).AddTicks(7556), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2014, 12, 7, 16, 31, 25, 964, DateTimeKind.Local).AddTicks(8365), "Gerald.Thiel@yahoo.com", "Hallie", "Halvorson", new DateTime(2021, 5, 28, 7, 26, 50, 987, DateTimeKind.Local).AddTicks(7953), 9 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2001, 2, 27, 2, 29, 4, 903, DateTimeKind.Local).AddTicks(9906), "Brant.Simonis@yahoo.com", "Anissa", "Senger", new DateTime(2021, 2, 27, 4, 12, 48, 62, DateTimeKind.Local).AddTicks(560), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2019, 5, 5, 5, 0, 46, 898, DateTimeKind.Local).AddTicks(7766), "Laury68@gmail.com", "Mac", "Little", new DateTime(2019, 12, 6, 11, 39, 29, 724, DateTimeKind.Local).AddTicks(3138), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2015, 5, 8, 3, 10, 30, 286, DateTimeKind.Local).AddTicks(3013), "Madie_Bashirian22@yahoo.com", "Camila", "Pagac", new DateTime(2019, 7, 27, 0, 46, 12, 890, DateTimeKind.Local).AddTicks(2804), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2017, 1, 11, 23, 58, 43, 258, DateTimeKind.Local).AddTicks(7664), "Clinton_Roob8@hotmail.com", "Kobe", "Stoltenberg", new DateTime(2021, 6, 6, 20, 10, 55, 802, DateTimeKind.Local).AddTicks(1217), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2014, 7, 21, 14, 55, 44, 296, DateTimeKind.Local).AddTicks(7345), "Celestino.Lesch32@gmail.com", "Axel", "Schultz", new DateTime(2020, 11, 20, 4, 50, 21, 653, DateTimeKind.Local).AddTicks(1773), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 1, 28, 2, 14, 46, 592, DateTimeKind.Local).AddTicks(6997), "Ruby0@yahoo.com", "Ernesto", "Swaniawski", new DateTime(2020, 7, 31, 18, 15, 10, 838, DateTimeKind.Local).AddTicks(430), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 6, 1, 17, 30, 15, 34, DateTimeKind.Local).AddTicks(216), "Art_Dooley@gmail.com", "Maude", "Heller", new DateTime(2020, 1, 7, 19, 5, 29, 823, DateTimeKind.Local).AddTicks(9727), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 11, 1, 9, 32, 52, 929, DateTimeKind.Local).AddTicks(9978), "Merritt_Sawayn@gmail.com", "Godfrey", "Metz", new DateTime(2021, 5, 27, 23, 43, 46, 227, DateTimeKind.Local).AddTicks(911), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2015, 12, 4, 19, 20, 6, 314, DateTimeKind.Local).AddTicks(1273), "Leonie38@yahoo.com", "Mustafa", "Kuphal", new DateTime(2019, 7, 21, 8, 2, 48, 422, DateTimeKind.Local).AddTicks(6229), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2019, 4, 18, 22, 42, 22, 384, DateTimeKind.Local).AddTicks(4098), "Timmy86@gmail.com", "Golden", "Hilll", new DateTime(2021, 3, 24, 4, 35, 9, 642, DateTimeKind.Local).AddTicks(6550), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2002, 5, 17, 16, 52, 22, 357, DateTimeKind.Local).AddTicks(8120), "Dolly.Rau89@hotmail.com", "Matilda", "Kuhic", new DateTime(2019, 10, 7, 3, 46, 48, 332, DateTimeKind.Local).AddTicks(1075), 11 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2020, 2, 28, 14, 27, 54, 735, DateTimeKind.Local).AddTicks(9072), "Cornelius_OKeefe90@gmail.com", "Rahsaan", "Marks", new DateTime(2020, 5, 30, 3, 4, 18, 888, DateTimeKind.Local).AddTicks(9735), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 11, 18, 9, 39, 28, 759, DateTimeKind.Local).AddTicks(531), "Layne78@yahoo.com", "Guadalupe", "Littel", new DateTime(2021, 2, 2, 22, 47, 27, 73, DateTimeKind.Local).AddTicks(3853), 11 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2016, 7, 14, 9, 29, 41, 894, DateTimeKind.Local).AddTicks(7003), "Declan_Graham@gmail.com", "Jocelyn", "Auer", new DateTime(2020, 8, 7, 15, 4, 28, 952, DateTimeKind.Local).AddTicks(6906), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1997, 6, 28, 15, 39, 17, 142, DateTimeKind.Local).AddTicks(9840), "Deondre88@hotmail.com", "Tavares", "Von", new DateTime(2021, 5, 8, 18, 37, 15, 550, DateTimeKind.Local).AddTicks(4090), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2005, 11, 17, 2, 21, 18, 817, DateTimeKind.Local).AddTicks(3727), "Kali66@gmail.com", "Andy", "Adams", new DateTime(2020, 2, 11, 19, 50, 15, 424, DateTimeKind.Local).AddTicks(1097), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2006, 9, 11, 6, 41, 8, 896, DateTimeKind.Local).AddTicks(6622), "Stevie_Strosin@hotmail.com", "Pamela", "Borer", new DateTime(2019, 7, 25, 17, 29, 12, 180, DateTimeKind.Local).AddTicks(3006), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2011, 7, 25, 9, 34, 6, 478, DateTimeKind.Local).AddTicks(8320), "Jamel5@hotmail.com", "Kareem", "Anderson", new DateTime(2020, 8, 20, 23, 11, 36, 599, DateTimeKind.Local).AddTicks(61), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2016, 7, 28, 13, 26, 27, 644, DateTimeKind.Local).AddTicks(7484), "Lisa83@hotmail.com", "Alberto", "Stamm", new DateTime(2019, 7, 18, 21, 57, 50, 617, DateTimeKind.Local).AddTicks(6306), 2 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2019, 5, 20, 23, 19, 2, 737, DateTimeKind.Local).AddTicks(3810), "Judson_Emard@gmail.com", "Annetta", "Skiles", new DateTime(2019, 8, 21, 23, 39, 57, 387, DateTimeKind.Local).AddTicks(1470), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2013, 6, 5, 4, 2, 15, 377, DateTimeKind.Local).AddTicks(6050), "Jermey94@hotmail.com", "Michael", "Schumm", new DateTime(2020, 11, 21, 7, 33, 41, 85, DateTimeKind.Local).AddTicks(2211), 7 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1996, 3, 6, 2, 34, 59, 364, DateTimeKind.Local).AddTicks(9266), "Jazmyne.Daniel@hotmail.com", "Marilou", "Nolan", new DateTime(2020, 2, 11, 14, 52, 33, 798, DateTimeKind.Local).AddTicks(5437), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2014, 8, 24, 21, 57, 24, 955, DateTimeKind.Local).AddTicks(9065), "Giovanny.Weimann38@yahoo.com", "Laurianne", "Stoltenberg", new DateTime(2021, 2, 5, 7, 22, 54, 330, DateTimeKind.Local).AddTicks(3955), 12 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2015, 1, 22, 13, 28, 7, 113, DateTimeKind.Local).AddTicks(2192), "Silas_Koepp@hotmail.com", "Marco", "Gusikowski", new DateTime(2021, 5, 29, 16, 5, 23, 152, DateTimeKind.Local).AddTicks(6988), 5 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1991, 1, 4, 3, 19, 24, 330, DateTimeKind.Local).AddTicks(7895), "Haley.Vandervort@yahoo.com", "Kaylin", "Treutel", new DateTime(2021, 5, 30, 7, 11, 4, 936, DateTimeKind.Local).AddTicks(2348), 14 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2003, 7, 16, 10, 38, 30, 79, DateTimeKind.Local).AddTicks(4921), "Dalton_Hackett86@hotmail.com", "Natalie", "Kunze", new DateTime(2020, 7, 6, 21, 6, 13, 934, DateTimeKind.Local).AddTicks(4857), 11 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1998, 12, 15, 18, 44, 28, 911, DateTimeKind.Local).AddTicks(5535), "Kyle_Stokes@yahoo.com", "Emmanuelle", "Sipes", new DateTime(2020, 11, 18, 4, 21, 14, 535, DateTimeKind.Local).AddTicks(4874), 6 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2018, 6, 21, 12, 0, 21, 516, DateTimeKind.Local).AddTicks(8020), "River_Bechtelar@gmail.com", "Kian", "Larson", new DateTime(2020, 3, 15, 16, 33, 34, 516, DateTimeKind.Local).AddTicks(541), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2018, 1, 14, 1, 11, 28, 84, DateTimeKind.Local).AddTicks(7389), "Trisha10@yahoo.com", "Tre", "Heaney", new DateTime(2019, 8, 25, 22, 2, 17, 345, DateTimeKind.Local).AddTicks(7284), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2009, 6, 20, 22, 1, 18, 847, DateTimeKind.Local).AddTicks(1756), "Amie_Stokes@hotmail.com", "Alva", "Ryan", new DateTime(2020, 9, 12, 0, 19, 32, 477, DateTimeKind.Local).AddTicks(7873), 1 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1982, 11, 24, 2, 38, 23, 473, DateTimeKind.Local).AddTicks(7294), "Stan_Yost16@hotmail.com", "Elisabeth", "Crona", new DateTime(2020, 11, 7, 10, 38, 4, 367, DateTimeKind.Local).AddTicks(8871), 3 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 77,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(1995, 10, 15, 21, 41, 15, 339, DateTimeKind.Local).AddTicks(3741), "Harold20@yahoo.com", "Vivienne", "Dickens", new DateTime(2019, 10, 1, 13, 44, 43, 717, DateTimeKind.Local).AddTicks(4556), 8 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 78,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 6, 26, 22, 21, 55, 403, DateTimeKind.Local).AddTicks(317), "Franco23@yahoo.com", "Joy", "Schoen", new DateTime(2020, 3, 22, 19, 24, 23, 110, DateTimeKind.Local).AddTicks(2154), 10 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 79,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2010, 11, 30, 19, 57, 58, 279, DateTimeKind.Local).AddTicks(726), "Aracely5@gmail.com", "Madge", "Anderson", new DateTime(2021, 3, 19, 16, 38, 48, 444, DateTimeKind.Local).AddTicks(5779), 4 });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 80,
                columns: new[] { "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[] { new DateTime(2016, 3, 14, 22, 30, 32, 994, DateTimeKind.Local).AddTicks(8983), "Bridgette.Cartwright@hotmail.com", "Adalberto", "Gerlach", new DateTime(2021, 3, 8, 18, 7, 59, 300, DateTimeKind.Local).AddTicks(2582), 4 });
        }
    }
}

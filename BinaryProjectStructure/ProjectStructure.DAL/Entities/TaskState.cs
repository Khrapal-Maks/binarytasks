﻿namespace ProjectStructure.DAL.Entities
{
    public enum TaskState
    {
        IsCreate,

        IsProgress,

        IsTesting,

        IsDone
    }
}

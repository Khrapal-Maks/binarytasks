﻿using Newtonsoft.Json;
using ProjectStructureClient.ModelsDTO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Timers;

namespace ProjectStructureClient.Services
{
    public class ReportsWebService
    {
        private const string API_PATH = "https://localhost:44322/api/Reports";

        private readonly HttpClient _client = new();
        private TaskCompletionSource<int> _completionSource;
        private Timer _timer;
        private readonly Random _random = new();

        public List<Task> Tasks { get; set; }

        public ReportsWebService()
        {
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            _random = new();
        }       

        private void GetTaskList()
        {
            var id = _random.Next(0, 100);

            Tasks = new();
            Tasks.Add(new Task(async () => await GetAllTasksInProjectsByAuthorFromApi(id)));
            Tasks.Add(new Task(async () => await GetAllTasksOnPerformerFromApi(id)));
            Tasks.Add(new Task(async () => await GetAllTasksThatFinishedFromApi(id)));
            Tasks.Add(new Task(async () => await GetAllUsersOldestThanTenYearsFromApi()));
            Tasks.Add(new Task(async () => await SortAllUsersFirstNameAndSortTaskOnNameFromApi()));
            Tasks.Add(new Task(async () => await GetStructUserByIdFromApi(id)));
            Tasks.Add(new Task(async () => await GetStructAllProjectsFromApi()));             
        }

        public async Task<List<Tuple<ProjectDTO, int>>> GetAllTasksInProjectsByAuthorFromApi(int id)
        { 
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/GetAllTasksInProjectsByAuthor/{id}");
            response.EnsureSuccessStatusCode();
          
            return JsonConvert.DeserializeObject<List<Tuple<ProjectDTO, int>>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<TasksDTO>> GetAllTasksOnPerformerFromApi(int id)
        {
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/GetAllTasksOnPerformer/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<TasksDTO>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<Tuple<int, string>>> GetAllTasksThatFinishedFromApi(int id)
        {
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/GetAllTasksThatFinished/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Tuple<int, string>>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<Tuple<int, string, List<UserDTO>>>> GetAllUsersOldestThanTenYearsFromApi()
        {
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/GetAllUsersOldestThanTenYears");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Tuple<int, string, List<UserDTO>>>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<Tuple<string, List<TasksDTO>>>> SortAllUsersFirstNameAndSortTaskOnNameFromApi()
        {
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/SortAllUsersFirstNameAndSortTaskOnName");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<Tuple<string, List<TasksDTO>>>>(await response.Content.ReadAsStringAsync());
        }

        public async Task<UserInfoDTO?> GetStructUserByIdFromApi(int id)
        {
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/GetStructUserById/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<UserInfoDTO?>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<ProjectInfoDTO>> GetStructAllProjectsFromApi()
        {
            HttpResponseMessage response = await _client.GetAsync($"{API_PATH}/GetStructAllProjects");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<List<ProjectInfoDTO>>(await response.Content.ReadAsStringAsync());
        }

        public Task<int> MarkRandomTaskWithDelay(int interval)
        {
            _timer = new Timer(interval)
            {
                AutoReset = true
            };
            _timer.Elapsed += Marking;
            _timer.Start();

            _completionSource = new();

            return _completionSource.Task;
        }

        private void Marking(object o, ElapsedEventArgs e)
        {
            try
            {
                GetTaskList();
                var task = Tasks.OrderBy(x => _random.Next(0, Tasks.Count)).First();
                task.Start();

                Debug.WriteLine($"Task Done{task.Id}");
                _completionSource.SetResult(task.Id);
            }
            catch (Exception ex)
            {
                Debug.WriteLine($"Exeption {ex.Message}");
                _completionSource.TrySetException(ex);
            }
        }
    }
}


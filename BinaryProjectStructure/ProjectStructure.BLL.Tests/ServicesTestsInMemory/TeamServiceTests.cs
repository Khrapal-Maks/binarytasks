﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.BLL.Services;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using ProjectStructure.DAL.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.BLL.Tests
{
    public class TeamServiceTests
    {
        private readonly NewTeamDTO _newTeam = new()
        {
            Name = "New team"
        };

        private IUnitOfWork _unitOf;
        private ITeamService _teamService;
        private IMapper _mapper;

        private readonly MapperConfiguration _mapperConfiguration = new(cfg =>
        {
            cfg.CreateMap<NewTeamDTO, Team>();
            cfg.CreateMap<TeamDTO, Team>();
            cfg.CreateMap<Team, TeamDTO>();
        });

        private readonly DbContextOptions<ProjectContext> _options = new DbContextOptionsBuilder<ProjectContext>()
                .UseInMemoryDatabase("TestTeam")
                .Options;

        [Fact]
        public async Task CreateTeam_ThanGetTeam()
        {
            //Arrange      
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _teamService = new TeamService(_unitOf, _mapper);

            //Act
            var expected = await _teamService.CreateTeam(_newTeam);
            var actual = await _teamService.GetTeam(expected.Id);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task CreateTeam_ThanUpdateTeam()
        {
            //Arrange
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _teamService = new TeamService(_unitOf, _mapper);

            //Act
            var actual = await _teamService.CreateTeam(_newTeam);

            actual = new TeamDTO
            {
                Id = actual.Id,
                Name = "Bast name",
                CreatedAt = actual.CreatedAt                
            };

            var expected = await _teamService.UpdateTeam(actual);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async Task CreateTask_ThanDeleteTask_ThanGetTask()
        {
            //Arrange
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _teamService = new TeamService(_unitOf, _mapper);

            //Act
            var team = await _teamService.CreateTeam(_newTeam);
            await _teamService.DeleteTeam(team.Id);
            var actual = await _teamService.GetTeam(team.Id);

            //Assert
            Assert.Null(actual);
        }

        [Fact]
        public async Task GetAllTask_ThanCreateNewTask_ThanGetAllTasks()
        {
            //Arrange
            using var _contextMemory = new ProjectContext(_options);
            _unitOf = new UnitOfWork(_contextMemory);
            _mapper = _mapperConfiguration.CreateMapper();
            _teamService = new TeamService(_unitOf, _mapper);

            //Act
            var expected = await _teamService.GetAllTeams();
            var task = await _teamService.CreateTeam(_newTeam);
            var actual = await _teamService.GetAllTeams();

            //Assert
            Assert.Equal(expected.Count() + 1, actual.Count());
        }
    }
}


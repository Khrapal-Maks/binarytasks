﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public sealed class TeamService : BaseService, ITeamService
    {
        private readonly IUnitOfWork _database;

        public TeamService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
            _database = context;
        }

        public async Task<IEnumerable<TeamDTO>> GetAllTeams()
        {
            var teams = await _database.GetRepositoryTeams.GetAll();

            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }

        public async Task<TeamDTO> GetTeam(int id)
        {
            var team = await _database.GetRepositoryTeams.Get(id);

            return _mapper.Map<TeamDTO>(team);
        }

        public async Task<TeamDTO> CreateTeam(NewTeamDTO newTeam)
        {
            var teamEntity = _mapper.Map<Team>(newTeam);

            var createToTeam = await _database.GetRepositoryTeams.Create(teamEntity);
            await _database.Save();

            var createdTeam = await _database.GetRepositoryTeams.Get(createToTeam.Id);

            return _mapper.Map<TeamDTO>(createdTeam);
        }

        public async Task<TeamDTO> UpdateTeam(TeamDTO team)
        {
            var updateTeam = _mapper.Map<Team>(team);

            await _database.GetRepositoryTeams.Update(team.Id, updateTeam);
            await _database.Save();

            var updatedTeam = await _database.GetRepositoryTeams.Get(team.Id);

            return _mapper.Map<TeamDTO>(updatedTeam);
        }

        public async Task DeleteTeam(int id)
        {
            await _database.GetRepositoryTeams.Delete(id);
            await _database.Save();
        }
    }
}

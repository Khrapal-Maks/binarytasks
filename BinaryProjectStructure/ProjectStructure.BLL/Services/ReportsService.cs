﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public sealed class ReportsService : BaseService, IReportsService
    {
        private readonly IUnitOfWork _database;

        public ReportsService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
            _database = context;
        }

        public async Task<List<Tuple<ProjectDTO, int>>> GetAllTasksInProjectsByAuthor(int authorId)
        {
            var projectsUsers = await _database.GetRepositoryProjects.FindIEnumerable(x => x.AuthorId == authorId);

            var allTasks = await _database.GetRepositoryTasks.GetAll();

            return projectsUsers.GroupJoin(allTasks,
                projects => projects.Id,
                tasks => tasks.ProjectId,
                (pr, ts) => new { Projects = pr, Tasks = ts })
                .Select(x => Tuple.Create(new ProjectDTO
                {
                    Id = x.Projects.Id,
                    AuthorId = x.Projects.AuthorId ?? 0,
                    CreatedAt = x.Projects.CreatedAt,
                    Deadline = x.Projects.Deadline,
                    Description = x.Projects.Description,
                    Name = x.Projects.Name,
                    TeamId = x.Projects.TeamId ?? 0
                }, x.Tasks.Count())).ToList();
        }

        public async Task<List<TasksDTO>> GetAllTasksOnPerformer(int performerId)
        {
            int lengthNameTask = 45;

            var result = await _database.GetRepositoryTasks.FindIEnumerable(x => x.PerformerId == performerId & x.Name.Length < lengthNameTask);

            return _mapper.Map<IEnumerable<TasksDTO>>(result).ToList();
        }

        public async Task<List<Tuple<int, string>>> GetAllTasksThatFinished(int performerId)
        {
            int year = 2021;

            var tasks =  await _database.GetRepositoryTasks.FindIEnumerable(x => x.PerformerId == performerId & x.FinishedAt.HasValue && x.FinishedAt.Value.Year == year);

            return tasks.Select(x => Tuple.Create(x.Id, x.Name)).ToList();
        }

        public async Task<List<Tuple<int, string, List<UserDTO>>>> GetAllUsersOldestThanTenYears()
        {
            int ageUser = 10;

            var teams = await _database.GetRepositoryTeams.GetAll();

            var users = await _database.GetRepositoryUsers.FindIEnumerable(x => x.BirthDay.AddYears(ageUser) < DateTime.Now);

            return teams.GroupJoin(users.OrderByDescending(x => x.RegisteredAt),
                team => team.Id,
                performer => performer.TeamId,
                (tm, pr) => new
                {
                    Team = tm,
                    Users = pr
                }).Select(x => Tuple.Create(x.Team, _mapper.Map<IEnumerable<UserDTO>>(x.Users).ToList())).Distinct()
                .Select(x => Tuple.Create(x.Item1.Id, x.Item1.Name, x.Item2.ToList())).ToList();
        }

        public async Task<List<Tuple<string, List<TasksDTO>>>> SortAllUsersFirstNameAndSortTaskOnName()
        {
            var users = await _database.GetRepositoryUsers.GetAll();

            var tasks = await _database.GetRepositoryTasks.GetAll();

            return users.OrderBy(x => x.FirstName).GroupJoin(tasks.OrderByDescending(x => x.Name.Length),
                performer => performer.Id,
                task => task.PerformerId,
                (pr, ts) => new { Performer = pr, Tasks = ts })
                .Select(x => Tuple.Create(x.Performer.FirstName, _mapper.Map<IEnumerable<TasksDTO>>(x.Tasks).ToList()))
                .Where(x => x.Item2.Any()).ToList();
        }

        public async Task<UserInfo?> GetStructUserById(int id)
        {
            var date = new DateTime(0001, 01, 01, 00, 00, 00);

            var user = await _database.GetRepositoryUsers.Get(id) ?? null;

            if (user == null)
            {
                return null;
            }

            var projects = await _database.GetRepositoryProjects.FindIEnumerable(x => x.AuthorId == user.Id);

            var project = projects.OrderByDescending(x => x.CreatedAt).FirstOrDefault() ?? new Project();

            var tasksProject = await _database.GetRepositoryTasks.FindIEnumerable(x => x.ProjectId == project.Id);

            var countTasksInProject = tasksProject.Count();

            var countTasksInWork = await _database.GetRepositoryTasks.GetAll();

            var tasksPerformer = await _database.GetRepositoryTasks.FindIEnumerable(x => x.PerformerId == user.Id);
                
             var task  = tasksPerformer.OrderByDescending(x => x.FinishedAt - x.CreatedAt).FirstOrDefault() ?? null;

            return new UserInfo
            {
                User = user,
                Project = project,
                CountTasksInProject = countTasksInProject,
                CountTasksInWork = countTasksInWork.Count(x => x.PerformerId == user.Id & x.FinishedAt != date),
                Tasks = task
            };
        }

        public async Task<List<ProjectInfo>> GetStructAllProjects()
        {
            int descriptionLength = 20;
            int countTask = 3;

            var projects = await _database.GetRepositoryProjects.GetAll();

            var tasks = await _database.GetRepositoryTasks.GetAll();

            var teams = await _database.GetRepositoryTeams.GetAll();

            var users = await _database.GetRepositoryUsers.GetAll();


            return await Task.Run(() => projects.GroupJoin(tasks,
                project => project.Id,
                task => task.ProjectId,
                (project, task) => new { Projects = project, Tasks = task })
                .Join(teams,
                project => project.Projects.TeamId,
                team => team.Id,
                (project, team) => new { Project = project, Team = team })
                .GroupJoin(users,
                project => project.Team.Id,
                user => user.TeamId,
                (project, user) => new ProjectInfo
                {
                    Project = project.Project.Projects,
                    TaskLongDescription = project.Project.Tasks.OrderBy(x => x.Description.Length).FirstOrDefault() ?? new Tasks(),
                    TasksShortName = project.Project.Tasks.OrderBy(x => x.Name.Length).FirstOrDefault() ?? new Tasks(),
                    CountUsersInTeamProject =
                    project.Project.Projects.Description.Length > descriptionLength | project.Project.Tasks.Count() < countTask
                    ? user.Count() : 0
                }).ToList());
        }

        public async Task<List<TasksDTO>> GetAllTasksIsNotDoneByUser(int id)
        {
            var result = await _database.GetRepositoryTasks.FindIEnumerable(x => x.PerformerId == id & x.FinishedAt == null);

            return _mapper.Map<List<TasksDTO>>(result);
        }
    }
}

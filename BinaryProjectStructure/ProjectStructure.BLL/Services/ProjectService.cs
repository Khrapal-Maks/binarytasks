﻿using AutoMapper;
using ProjectStructure.BLL.ModelsDTO;
using ProjectStructure.DAL.Entities;
using ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectStructure.BLL.Services
{
    public sealed class ProjectService : BaseService, IProjectService
    {
        private readonly IUnitOfWork _database;

        public ProjectService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {
            _database = context;
        }

        public async Task<IEnumerable<ProjectDTO>> GetAllProjects()
        {
            var projects = await _database.GetRepositoryProjects.GetAll();

            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }

        public async Task<ProjectDTO> GetProject(int id)
        {
            var project = await _database.GetRepositoryProjects.Get(id);

            return _mapper.Map<ProjectDTO>(project);
        }

        public async Task<ProjectDTO> CreateProject(NewProjectDTO project)
        {
            var projectEntity = _mapper.Map<Project>(project);

            var createToProject = await _database.GetRepositoryProjects.Create(projectEntity);
            await _database.Save();

            var createdProject = await _database.GetRepositoryProjects.Get(createToProject.Id);

            return _mapper.Map<ProjectDTO>(createdProject);
        }

        public async Task<ProjectDTO> UpdateProject(ProjectDTO project)
        {
            var updateProject = _mapper.Map<Project>(project);

            await _database.GetRepositoryProjects.Update(updateProject.Id, updateProject);
            await _database.Save();

            var updatedProject = await _database.GetRepositoryProjects.Get(project.Id);

            return _mapper.Map<ProjectDTO>(updatedProject);
        }

        public async Task DeleteProject(int id)
        {
            await _database.GetRepositoryProjects.Delete(id);
            await _database.Save();
        }
    }
}
